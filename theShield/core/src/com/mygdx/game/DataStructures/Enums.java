package com.mygdx.game.DataStructures;

/**
 * Created by Solist on 25/03/2017.
 */

public class Enums {

    public enum AttackType {
        MELEE,
        RANGED
    }

    public enum Direction {
        NORTH,
        EAST,
        SOUTH,
        WEST
    }

    public enum SpriteDirection {
        UP,
        DOWN,
        LEFT,
        RIGHT
    }
}
