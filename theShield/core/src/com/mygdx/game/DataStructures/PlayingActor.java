package com.mygdx.game.DataStructures;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g3d.particles.influencers.ColorInfluencer;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.game.AI.Location;
import com.mygdx.game.Blessings.Blessing;
import com.mygdx.game.Controllers.GameController;
import com.mygdx.game.Levels.Level;
import com.mygdx.game.Projectiles.Arrow;
import com.mygdx.game.Projectiles.Projectile;

import org.lwjgl.util.glu.Project;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Solist on 26/03/2017.
 */

public abstract class PlayingActor extends Actor {

    // Variables.
    private boolean cancelledCollisonOnDeath = false;

    // Stats.
    public int lives = 1;
    public int baseTotalLives = 1;
    public int totalLivesBuff = 0;
    public int totalLives = 1;
    public boolean livesChanged = false;
    public boolean isDead = false;
    public String killedBy;

    public int creaturesKilled = 0;

    public Blessing[] blessings;
    public int maxBlessings = 30;
    public int currentBlessings = 0;

    public double baseMovementSpeed = 0.5;
    public double movementSpeedBuff = 0; // In percent. 55.68% sort of style.
    public double movementSpeed = 0.5;
    public boolean isMoving = false;
    public double baseRotationSpeed = 0.3;
    public double rotationSpeed = 0.3;
    public boolean isRotatingLeft = false;
    public boolean isRotatingRight = false;

    public Enums.AttackType attackMode = Enums.AttackType.MELEE;
    public int baseAttackDamage = 1;
    public int attackDamageBuff = 0; // In flat numbers to multiply by, value of 2 = 2x damage.
    public int attackDamage = 1;
    public float attackStartTime = 0;
    public double baseAttackSpeed = 0.5;
    public double attackSpeedBuff = 0; // In percent. 55.68% sort of style.
    public double attackSpeed = 0.5;
    public boolean isAttacking = false;
    public boolean hasHit = false;
    public int baseAttackRange = 1;
    public int attackRangeBuff = 0; // In flat numbers to multiply by, value of 2 = 2x range.
    public int attackRange = 1;
    public String projectileType;
    public String projectileFilePath = "com.mygdx.game.Projectiles.";

    public float blockStartTime = 0;
    public double baseBlockSpeed = 0.4;
    public double blockSpeedBuff = 0; // In Percent. 55.68% sort of stlye.
    public double blockSpeed = 0.4;
    public boolean blockReady = false;
    public boolean blockSoundActive = false;
    public boolean isBlocking = false;
    public boolean blockCanceled = false;
    public boolean blockFinished = false;

    public float switchWeaponStartTime = 0;
    public float switchWeaponCurrentTime = 0;
    public double switchWeaponSpeed = 1.0;
    public boolean isSwitchingWeapons = false;

    public boolean isSwappingWeapons = false;

    public float currentDelayTimer;

    public float startTime = 0;
    public float currentTime = 0;
    public float previousTime = 0;

    private Vector2 startPosition = new Vector2();
    public int cellX;
    public int cellY;
    public float startRotation = 0;
    public float rotation = 0;

    // Sounds.
    public Sound[] weaponSounds;
    public boolean[] movementSoundIntervals;

    //AI variables
    public int detectRange = 0;
    public Boolean detectTraps = false;
    public ArrayList<Location> path = new ArrayList<Location>();


    public PlayingActor(GameController gameController, String playerName, Texture texture, Enums.Direction  direction, Level level) {
        super(texture, new Vector2(128, 128));

        this.gameController = gameController;

        playerObject = (RectangleMapObject)level.playerLayer.getObjects().get(playerName);
        playerObject.getRectangle().setCenter(playerObject.getRectangle().x + 128, playerObject.getRectangle().y + 128);
        startPosition.x = playerObject.getRectangle().x;
        startPosition.y = playerObject.getRectangle().y;
        Position = startPosition;

        Direction = direction;

        switch(Direction) {
            case NORTH:
                rotation = 0;
                startRotation = rotation;
                break;
            case EAST:
                rotation = 90;
                startRotation = rotation;
                break;
            case SOUTH:
                rotation = 180;
                startRotation = rotation;
                break;
            case WEST:
                rotation = 270;
                startRotation = rotation;
                break;
            default:
                break;
        }

        blessings = new Blessing[maxBlessings];
    }


    // Methods.

    // Movement.
    public void MoveForward(float delta) {
        // Smoothly move the player forward 1 unit (square).
        currentTime += delta;
        if (currentTime - startTime <= movementSpeed) {
            switch (Direction) {
                case NORTH:
                    Position = new Vector2(startPosition.x, startPosition.y + (float)(128 * (currentTime - startTime) / movementSpeed));
                    break;
                case EAST:
                    Position = new Vector2(startPosition.x  + (float)(128 * (currentTime - startTime) / movementSpeed), startPosition.y);
                    break;
                case SOUTH:
                    Position = new Vector2(startPosition.x, startPosition.y - (float)(128 * (currentTime - startTime) / movementSpeed));
                    break;
                case WEST:
                    Position = new Vector2(startPosition.x - (float)(128 * (currentTime - startTime) / movementSpeed), startPosition.y);
                    break;
                default:
                    break;
            }

            playerObject.getRectangle().x = Position.x;
            playerObject.getRectangle().y = Position.y;
        }
        else if (isMoving == true){
            isMoving = false;
            // fixPosition() method is used to update the position to the correct position as the
            // movement over time may not put us at the desired location.
            fixPosition();
        }
    }

    // Rotation.
    public void TurnLeft(float delta) {
        // Play the rotate character Left sprite.

        // Change the characters direction
        // (i.e. if the character's current direction is NORTH, change their Direction to WEST).
        currentTime += delta;
        if (currentTime - startTime <= rotationSpeed) {
            rotation -= startRotation * (currentTime - startTime) / rotationSpeed;
            if (rotation < 0) {
                rotation = 360 - rotation;
            }
        }
        else if (isRotatingLeft == true) {
            isRotatingLeft = false;
            switch (Direction) {
                case NORTH:
                    Direction = Enums.Direction.WEST;
                    break;
                case EAST:
                    Direction = Enums.Direction.NORTH;
                    break;
                case SOUTH:
                    Direction = Enums.Direction.EAST;
                    break;
                case WEST:
                    Direction = Enums.Direction.SOUTH;
                    break;
                default:
                    break;
            }
            fixRotation();
        }
    }

    public void TurnRight(float delta) {
        // Play the rotate character Right sprite.
        // Change the characters direction
        // (i.e. if the character's current direction is NORTH, change their Direction to EAST).
        currentTime += delta;
        if (currentTime - startTime <= rotationSpeed) {
            rotation += startRotation * (currentTime - startTime) / rotationSpeed;
            if (rotation >= 360) {
                rotation = rotation - 360;
            }
        }
        else if (isRotatingRight == true){
            isRotatingRight = false;
            switch (Direction) {
                case NORTH:
                    Direction = Enums.Direction.EAST;
                    break;
                case EAST:
                    Direction = Enums.Direction.SOUTH;
                    break;
                case SOUTH:
                    Direction = Enums.Direction.WEST;
                    break;
                case WEST:
                    Direction = Enums.Direction.NORTH;
                    break;
                default:
                    break;
            }
            fixRotation();
        }
    }


    // Combat.
    public void MeleeAttack(float delta) {
        if (attackMode == Enums.AttackType.MELEE && performingActions() == false) {
            isAttacking = true;
            attackStartTime = delta;
            currentDelayTimer = delta;
            // The player can attack.
            // Swing sword animation.

            float effectVolume = getEffectVolume();

            Random rand = new Random();

            // Player attack sound.
            int randInt = rand.nextInt(playerSounds[0].length);
            long playerGruntID = playerSounds[0][randInt].play();
            playerSounds[0][randInt].setVolume(playerGruntID, effectVolume);

            // Attack swing sound.
            long weaponSoundID = weaponSounds[0].play();
            weaponSounds[0].setVolume(weaponSoundID, effectVolume);
        }
        else if(currentDelayTimer <= attackStartTime + attackSpeed){
            currentDelayTimer += delta;
            // Play animation.


            // Check if hit can be performed yet.
            if (hasHit == false && currentDelayTimer >= attackStartTime + (attackSpeed / 4)) {
                hasHit = true;
                // Perform hit.
                Vector2 hitPosition = new Vector2(cellX * 128 + 64, cellY * 128 + 64);
                PlayingActor target;
                switch (Direction) {
                    case NORTH:
                        hitPosition.y += 128;
                        target = gameController.collisions.checkPlayerCollision(hitPosition);
                        if (target != null){
                            if (target.blockReady == false || checkTargetFacingUs(target) == false) {
                                target.ReceiveDamage(attackDamage, this);
                            }
                            else {
                                long soundID = target.weaponSounds[1].play();
                                target.weaponSounds[1].setVolume(soundID, getEffectVolume());
                            }
                            break;
                        }
                        target = gameController.collisions.checkEnemyCollision(hitPosition);
                        if (target != null) {
                            if (target.blockReady == false || checkTargetFacingUs(target) == false) {
                                target.ReceiveDamage(attackDamage, this);
                            }
                            else {
                                // Play the blocked sound.
                                long soundID = target.weaponSounds[1].play();
                                target.weaponSounds[1].setVolume(soundID, getEffectVolume());
                            }
                            break;
                        }
                        break;
                    case EAST:
                        hitPosition.x += 128;
                        target = gameController.collisions.checkPlayerCollision(hitPosition);
                        if (target != null){
                            if (target.blockReady == false || checkTargetFacingUs(target) == false) {
                                target.ReceiveDamage(attackDamage, this);
                            }
                            else {
                                // Play the blocked sound.
                                long soundID = target.weaponSounds[1].play();
                                target.weaponSounds[1].setVolume(soundID, getEffectVolume());
                            }
                            break;
                        }
                        target = gameController.collisions.checkEnemyCollision(hitPosition);
                        if (target != null) {
                            if (target.blockReady == false || checkTargetFacingUs(target) == false) {
                                target.ReceiveDamage(attackDamage, this);
                            }
                            else {
                                // Play the blocked sound.
                                long soundID = target.weaponSounds[1].play();
                                target.weaponSounds[1].setVolume(soundID, getEffectVolume());
                            }
                            break;
                        }
                        break;
                    case SOUTH:
                        hitPosition.y -= 128;
                        target = gameController.collisions.checkPlayerCollision(hitPosition);
                        if (target != null){
                            if (target.blockReady == false || checkTargetFacingUs(target) == false) {
                                target.ReceiveDamage(attackDamage, this);
                            }
                            else {
                                // Play the blocked sound.
                                long soundID = target.weaponSounds[1].play();
                                target.weaponSounds[1].setVolume(soundID, getEffectVolume());
                            }
                            break;
                        }
                        target = gameController.collisions.checkEnemyCollision(hitPosition);
                        if (target != null) {
                            if (target.blockReady == false || checkTargetFacingUs(target) == false) {
                                target.ReceiveDamage(attackDamage, this);
                            }
                            else {
                                // Play the blocked sound.
                                long soundID = target.weaponSounds[1].play();
                                target.weaponSounds[1].setVolume(soundID, getEffectVolume());
                            }
                            break;
                        }
                        break;
                    case WEST:
                        hitPosition.x -= 128;
                        target = gameController.collisions.checkPlayerCollision(hitPosition);
                        if (target != null){
                            if (target.blockReady == false || checkTargetFacingUs(target) == false) {
                                target.ReceiveDamage(attackDamage, this);
                            }
                            else {
                                // Play the blocked sound.
                                long soundID = target.weaponSounds[1].play();
                                target.weaponSounds[1].setVolume(soundID, getEffectVolume());
                            }
                            break;
                        }
                        target = gameController.collisions.checkEnemyCollision(hitPosition);
                        if (target != null) {
                            if (target.blockReady == false || checkTargetFacingUs(target) == false) {
                                target.ReceiveDamage(attackDamage, this);
                            }
                            else {
                                // Play the blocked sound.
                                long soundID = target.weaponSounds[1].play();
                                target.weaponSounds[1].setVolume(soundID, getEffectVolume());
                            }
                            break;
                        }
                        break;
                    default:
                        break;
                }
            }
        }
        else {
            isAttacking = false;
            hasHit = false;
            // Display to the player that they can't perform an attack right now.

        }
    }

    public void RangedAttack(float delta) {
        if (attackMode == Enums.AttackType.RANGED && performingActions() == false) {
            isAttacking = true;
            attackStartTime = delta;
            currentDelayTimer = delta;
            // The player can attack.
        } else if (currentDelayTimer <= attackStartTime + attackSpeed) {
            currentDelayTimer += delta;
            // Play animation.


            // Check if hit can be performed yet.
            if (hasHit == false && currentDelayTimer >= attackStartTime + (attackSpeed / 2)) {
                hasHit = true;
                // Find an empty projectile slot and create a new projectile.
                for (int i = 0; i < gameController.level.projectileList.length; i++) {
                    if (gameController.level.projectileList[i] == null) {
                        long soundID = weaponSounds[2].play();
                        weaponSounds[2].setVolume(soundID, getEffectVolume());
                        try {
                            String jointFilePath = projectileFilePath + projectileType;
                            Class<?> projectileClass = Class.forName(jointFilePath);
                            Constructor<?> projectileConstructor = projectileClass.getConstructor(GameController.class, int.class,
                                    PlayingActor.class, float.class, Vector2.class, Enums.Direction.class);
                            Object projectileObject;
                            Projectile projectile;
                            switch (Direction) {
                                case NORTH:
                                    projectileObject = projectileConstructor.newInstance(gameController, i,
                                            this, delta, new Vector2(Position.x, Position.y + 64), Direction);
                                    projectile = ((Projectile) projectileObject);
                                    gameController.level.projectileList[i] = projectile;

                                    //gameController.level.projectileList[i] = projectile(gameController, i,
                                    //        this, delta, new Vector2(Position.x, Position.y + 64), Direction);
                                    break;
                                case EAST:
                                    projectileObject = projectileConstructor.newInstance(gameController, i,
                                            this, delta, new Vector2(Position.x + 64, Position.y), Direction);
                                    projectile = ((Projectile) projectileObject);
                                    gameController.level.projectileList[i] = projectile;

                                    //gameController.level.projectileList[i] = new Arrow(gameController, i,
                                    //        this, delta, new Vector2(Position.x + 64, Position.y), Direction);
                                    break;
                                case SOUTH:
                                    projectileObject = projectileConstructor.newInstance(gameController, i,
                                            this, delta, new Vector2(Position.x, Position.y - 64), Direction);
                                    projectile = ((Projectile) projectileObject);
                                    gameController.level.projectileList[i] = projectile;

                                    //gameController.level.projectileList[i] = new Arrow(gameController, i,
                                    //        this, delta, new Vector2(Position.x, Position.y - 64), Direction);
                                    break;
                                case WEST:
                                    projectileObject = projectileConstructor.newInstance(gameController, i,
                                            this, delta, new Vector2(Position.x - 64, Position.y), Direction);
                                    projectile = ((Projectile) projectileObject);
                                    gameController.level.projectileList[i] = projectile;

                                    //gameController.level.projectileList[i] = new Arrow(gameController, i,
                                    //        this, delta, new Vector2(Position.x - 64, Position.y), Direction);
                                    break;
                            }
                            break;
                        } catch (Exception e) {

                        }
                    }
                }
            }
        }
        else {
            isAttacking = false;
            hasHit = false;
        }
    }

    public void Block(float delta) {
        if (attackMode == attackMode.MELEE) {
            if (blockFinished) {
                currentDelayTimer += delta;

                if (currentDelayTimer <= blockStartTime + blockSpeed) {
                    if (blockSoundActive == false) {
                        long weaponSoundID = weaponSounds[0].play();
                        weaponSounds[0].setVolume(weaponSoundID, getEffectVolume());
                        blockSoundActive = true;
                    }
                    // Display block drop animation.
                } else {
                    isBlocking = false;
                    blockSoundActive = false;
                    blockFinished = false;
                }
            } else if (performingActions() == false) {
                isBlocking = true;
                blockStartTime = delta;
                currentDelayTimer = delta;
                // Display Block raise animation.
                //currentSpriteIndex = 2;


                float effectVolume = getEffectVolume();
                // Block swing sound.
                long weaponSoundID = weaponSounds[0].play();
                weaponSounds[0].setVolume(weaponSoundID, effectVolume);
            } else {
                if (blockCanceled && currentDelayTimer > blockStartTime + blockSpeed) {
                    currentDelayTimer = delta;
                    blockStartTime = delta;
                    blockFinished = true;
                    blockCanceled = false;
                    blockReady = false;
                    blockSoundActive = false;
                }
                else if (currentDelayTimer <= blockStartTime + blockSpeed) {
                    currentDelayTimer += delta;
                    currentAnimationTime = currentDelayTimer - blockStartTime;
                    // Play the Block animation.
                } else if (currentDelayTimer > blockStartTime + blockSpeed) {
                    currentDelayTimer += delta;
                    blockReady = true;
                } else {
                    // Display to the player that they can't perform a block right now.
                }
            }
        }
    }


    public void SwitchSwordAndShield() {
        attackMode = Enums.AttackType.MELEE;
        currentSpriteIndex = 0;

        float effectVolume = getEffectVolume();
        // Play sword and shield swap sound.
        long soundID1 = weaponSounds[5].play();
        weaponSounds[5].setVolume(soundID1, effectVolume);
        long soundID2 = weaponSounds[4].play();
        weaponSounds[4].setVolume(soundID2, effectVolume);
        // Play sword and shield swap animation.
    }

    public void SwitchBowAndArrow() {
        attackMode = Enums.AttackType.RANGED;
        currentSpriteIndex = 1;

        float effectVolume = getEffectVolume();
        // Play sword and shield swap sound.
        long soundID1 = weaponSounds[3].play();
        weaponSounds[3].setVolume(soundID1, effectVolume);
        long soundID2 = weaponSounds[5].play();
        weaponSounds[5].setVolume(soundID2, effectVolume);
        // Play bow and arrow swap animation.
    }


    public void ReceiveDamage(int damage, Actor attacker) {
        float effectVolume = getEffectVolume();

        if (damage > lives) {
            lives = 0;
        }
        else {
            lives -= damage;
        }

        Random rand = new Random();

        int randInt = rand.nextInt(playerSounds[1].length);
        long hitSoundID = playerSounds[1][randInt].play();
        playerSounds[1][randInt].setVolume(hitSoundID, effectVolume);

        randInt = rand.nextInt(playerSounds[2].length);
        hitSoundID = playerSounds[2][randInt].play();
        playerSounds[2][randInt].setVolume(hitSoundID, effectVolume);

        if (lives <= 0) {
            if (attacker instanceof PlayingActor) {
                PlayingActor playingActor = (PlayingActor) attacker;
                if (playingActor != null) {
                    playingActor.creaturesKilled++;
                }
            }
            isDead = true;
            // Play death sound.
            randInt = rand.nextInt(playerSounds[3].length);
            hitSoundID = playerSounds[3][randInt].play();
            playerSounds[3][randInt].setVolume(hitSoundID, effectVolume);
            killedBy = attacker.actorName;
        }

        livesChanged = true;
    }

    public void ReceiveHealing(int healing) {
        if (lives + healing > totalLives) {
            lives = totalLives;
        }
        else {
            lives += healing;
        }

        livesChanged = true;
    }


    public void fixPosition() {
        startPosition.x = Math.round(startPosition.x);
        startPosition.y = Math.round(startPosition.y);
        cellX = (int)((startPosition.x - 64) / 128);
        cellY = (int)((startPosition.y - 64) / 128);
        switch (Direction){
            case NORTH:
                Position.y = startPosition.y + 128;
                cellY += 1;
                break;
            case EAST:
                Position.x = startPosition.x + 128;
                cellX += 1;
                break;
            case SOUTH:
                Position.y = startPosition.y - 128;
                cellY -= 1;
                break;
            case WEST:
                Position.x = startPosition.x - 128;
                cellX -= 1;
                break;
            default:
                break;
        }
        playerObject.getRectangle().x = Position.x;
        playerObject.getRectangle().y = Position.y;
        gameController.level.positionsFlaggedForMoving[cellX][cellY] = false;
    }


    public void fixRotation() {
        switch (Direction) {
            case NORTH:
                rotation = 0;
                startRotation = 0;
                break;
            case EAST:
                rotation = 90;
                startRotation = 90;
                break;
            case SOUTH:
                rotation = 180;
                startRotation = 180;
                break;
            case WEST:
                rotation = 270;
                startRotation = 270;
                break;
            default:
                break;
        }
    }


    public void setStartTime(float startTime) {
        this.startTime = startTime;
        currentTime = startTime;
        previousTime = startTime;
        currentAnimationTime = 0;
    }

    public void setStartingPosition(){
        startPosition = Position;
    }


    // Add a Blessing.
    public void addBlessing(Blessing blessing) {
        for (int i = 0; i < blessings.length; i++) {
            if (blessings[i] == null) {
                blessings[i] = blessing;
                currentBlessings++;
                blessing = blessings[i];
                break;
            }
        }
        updateBuffs();

        if (blessing.healing > 0) {
            ReceiveHealing(blessing.healing);
            blessing.healing = 0;

            livesChanged = true;
        }
    }

    public void updateBuffs() {
        resetBuffs();

        for (int i = 0; i < blessings.length; i++) {
            Blessing blessing = blessings[i];
            if (blessing != null) {
                blockSpeedBuff += blessing.attackSpeed;

                attackDamageBuff += blessing.damage;
                attackRangeBuff += blessing.range;
                attackSpeedBuff += blessing.attackSpeed;

                movementSpeedBuff += blessing.movementSpeed;

                totalLivesBuff += blessing.lives;
            }
        }

        double tempBlockSpeedBonus = 0;
        while (blockSpeedBuff > 0) {
            if (blockSpeedBuff >= 30) {
                tempBlockSpeedBonus += baseBlockSpeed * (30.0 / 100.0);
                blockSpeedBuff -= 30;
                if (blockSpeedBuff >= 15) {
                    tempBlockSpeedBonus += baseBlockSpeed * (15.0 / 200.0);
                    blockSpeedBuff -= 15;
                    if (blockSpeedBuff >= 7.5) {
                        tempBlockSpeedBonus += baseBlockSpeed * (7.5 / 300.0);
                        blockSpeedBuff -= 7.5;
                        while (blockSpeedBuff > 4) {
                            tempBlockSpeedBonus += baseBlockSpeed * (4.0 / 400.0);
                            blockSpeedBuff -= 4;
                        }
                        if (blockSpeedBuff > 0) {
                            tempBlockSpeedBonus += baseBlockSpeed * (blockSpeedBuff / 400.0);
                            blockSpeedBuff -= blockSpeedBuff;
                        }
                    }
                    else {
                        tempBlockSpeedBonus += baseBlockSpeed * (blockSpeedBuff / 300.0);
                        blockSpeedBuff -= blockSpeedBuff;
                    }
                }
                else {
                    tempBlockSpeedBonus += baseBlockSpeed * (blockSpeedBuff / 200.0);
                    blockSpeedBuff -= blockSpeedBuff;
                }
            }
            else {
                tempBlockSpeedBonus += baseBlockSpeed * (blockSpeedBuff / 100.0);
                blockSpeedBuff -= blockSpeedBuff;
            }
        }
        blockSpeed = baseBlockSpeed - tempBlockSpeedBonus;


        attackDamage = baseAttackDamage + (baseAttackDamage * attackDamageBuff);
        attackRange = baseAttackRange + (baseAttackDamage * attackDamageBuff);

        double tempAttackSpeedBonus = 0;
        while (attackSpeedBuff > 0) {
            if (attackSpeedBuff >= 30) {
                tempAttackSpeedBonus += baseAttackSpeed * (30.0 / 100.0);
                attackSpeedBuff -= 30;
                if (attackSpeedBuff >= 15) {
                    tempAttackSpeedBonus += baseAttackSpeed * (15.0 / 200.0);
                    attackSpeedBuff -= 15;
                    if (attackSpeedBuff >= 7.5) {
                        tempAttackSpeedBonus += baseAttackSpeed * (7.5 / 300.0);
                        attackSpeedBuff -= 7.5;
                        while (attackSpeedBuff > 4) {
                            tempAttackSpeedBonus += baseAttackSpeed * (4.0 / 400.0);
                            attackSpeedBuff -= 4;
                        }
                        if (attackSpeedBuff > 0) {
                            tempAttackSpeedBonus += baseAttackSpeed * (attackSpeedBuff / 400.0);
                            attackSpeedBuff -= attackSpeedBuff;
                        }
                    }
                    else {
                        tempAttackSpeedBonus += baseAttackSpeed * (attackSpeedBuff / 300.0);
                        attackSpeedBuff -= attackSpeedBuff;
                    }
                }
                else {
                    tempAttackSpeedBonus += baseAttackSpeed * (attackSpeedBuff / 200.0);
                    attackSpeedBuff -= attackSpeedBuff;
                }
            }
            else {
                tempAttackSpeedBonus += baseAttackSpeed * (attackSpeedBuff / 100.0);
                attackSpeedBuff -= attackSpeedBuff;
            }
        }
        attackSpeed = baseAttackSpeed - tempAttackSpeedBonus;


        double tempMoveSpeedBuff = movementSpeedBuff;
        double tempMoveSpeedBonus = 0;
        while (tempMoveSpeedBuff > 0) {
            if (tempMoveSpeedBuff >= 30) {
                tempMoveSpeedBonus += baseMovementSpeed * (30.0 / 100.0);
                tempMoveSpeedBuff -= 30;
                if (tempMoveSpeedBuff >= 15) {
                    tempMoveSpeedBonus += baseMovementSpeed * (15.0 / 200.0);
                    tempMoveSpeedBuff -= 15;
                    if (tempMoveSpeedBuff >= 7.5) {
                        tempMoveSpeedBonus += baseMovementSpeed * (7.5 / 300.0);
                        tempMoveSpeedBuff -= 7.5;
                        while (tempMoveSpeedBuff > 4) {
                            tempMoveSpeedBonus += baseMovementSpeed * (4.0 / 400.0);
                            tempMoveSpeedBuff -= 4;
                        }
                        if (tempMoveSpeedBuff > 0) {
                            tempMoveSpeedBonus += baseMovementSpeed * (tempMoveSpeedBuff / 400.0);
                            tempMoveSpeedBuff -= tempMoveSpeedBuff;
                        }
                    }
                    else {
                        tempMoveSpeedBonus += baseMovementSpeed * (tempMoveSpeedBuff / 300.0);
                        tempMoveSpeedBuff -= tempMoveSpeedBuff;
                    }
                }
                else {
                    tempMoveSpeedBonus += baseMovementSpeed * (tempMoveSpeedBuff / 200.0);
                    tempMoveSpeedBuff -= tempMoveSpeedBuff;
                }
            }
            else {
                tempMoveSpeedBonus += baseMovementSpeed * (movementSpeedBuff / 100.0);
                movementSpeedBuff -= movementSpeedBuff;
            }
        }
        movementSpeed = baseMovementSpeed - tempMoveSpeedBonus;


        double tempRotationSpeedBuff = movementSpeedBuff;
        float tempRotationSpeedBonus = 0;
        while (tempRotationSpeedBuff > 0) {
            if (tempRotationSpeedBuff >= 30) {
                tempRotationSpeedBonus += baseRotationSpeed * (30.0 / 100.0);
                tempRotationSpeedBuff -= 30;
                if (tempRotationSpeedBuff >= 15) {
                    tempRotationSpeedBonus += baseRotationSpeed * (15.0 / 200.0);
                    tempRotationSpeedBuff -= 15;
                    if (tempRotationSpeedBuff >= 7.5) {
                        tempRotationSpeedBonus += baseRotationSpeed * (7.5 / 300.0);
                        tempRotationSpeedBuff -= 7.5;
                        while (tempRotationSpeedBuff > 4) {
                            tempRotationSpeedBonus += baseRotationSpeed * (4.0 / 400.0);
                            tempRotationSpeedBuff -= 4;
                        }
                        if (tempRotationSpeedBuff > 0) {
                            tempRotationSpeedBonus += baseRotationSpeed * (tempRotationSpeedBuff / 400.0);
                            tempRotationSpeedBuff -= tempRotationSpeedBuff;
                        }
                    }
                    else {
                        tempRotationSpeedBonus += baseRotationSpeed * (tempRotationSpeedBuff / 300.0);
                        tempRotationSpeedBuff -= tempRotationSpeedBuff;
                    }
                }
                else {
                    tempRotationSpeedBonus += baseRotationSpeed * (tempRotationSpeedBuff / 200.0);
                    tempRotationSpeedBuff -= tempRotationSpeedBuff;
                }
            }
            else {
                tempRotationSpeedBonus += baseRotationSpeed * (tempRotationSpeedBuff / 100.0);
                tempRotationSpeedBuff -= tempRotationSpeedBuff;
            }
        }
        rotationSpeed = baseRotationSpeed - tempRotationSpeedBonus;


        totalLives = baseTotalLives + totalLivesBuff;
        /*livesChanged = true;*/
    }

    public void resetBuffs() {
        blockSpeedBuff = 0;

        attackDamageBuff = 0;
        attackRangeBuff = 0;
        attackSpeedBuff = 0;

        movementSpeedBuff = 0;

        totalLivesBuff = 0;
    }


    // Checks.
    public boolean checkTargetFacingUs(PlayingActor target) {
        switch(Direction) {
            case NORTH:
                if (target.Direction != Direction.SOUTH) {
                    return false;
                }
                break;
            case EAST:
                if (target.Direction != Direction.WEST) {
                    return false;
                }
                break;
            case SOUTH:
                if (target.Direction != Direction.NORTH) {
                    return false;
                }
                break;
            case WEST:
                if (target.Direction != Direction.EAST) {
                    return false;
                }
                break;
            default:
                break;
        }
        return true;
    }

    public boolean performingActions() {
        if (isAttacking == true || isBlocking == true ||
                isMoving == true || isRotatingLeft == true ||
                isRotatingRight == true || isSwappingWeapons == true) {
            return true;
        }
        return false;
    }


    @Override
    public void render(float delta) {
        super.render(delta);
        if (cancelledCollisonOnDeath == false && isDead == true) {
            cancelledCollisonOnDeath = true;
            gameController.level.positionsFlaggedForMoving[cellX][cellY] = false;
        }
        if (isDead == false) {
            boolean buffsChanged = false;
            for (int i = 0; i < blessings.length; i++) {
                if (blessings[i] != null && blessings[i].duration != -1) {
                    if (blessings[i].duration - delta <= 0) {
                        blessings[i] = null;
                        currentBlessings--;
                        buffsChanged = true;
                    }
                    else {
                        blessings[i].duration -= delta;
                    }
                }
            }
            if (buffsChanged == true) {
                updateBuffs();
            }
        }
    }
}
