package com.mygdx.game.DataStructures;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.mygdx.game.Controllers.GameController;
import com.mygdx.game.Levels.Level;

/**
 * Created by Solist on 3/06/2017.
 */

public class NPCActor extends PlayingActor {

    enum SpriteState {
        // Idle states.
        IdleFront,
        IdleBack,
        IdleLeft,
        IdleRight,

        // Moving states.
        MoveUp,
        MoveDown,
        MoveLeft,
        MoveRight,

        // Rotation states.
        TurnLeftFromFront,
        TurnRightFromFront,
        TurnLeftFromBack,
        TurnRightFromBack,
        TurnLeftFromLeft,
        TurnRightFromLeft,
        TurnLeftFromRight,
        TurnRightFromRight,

        // Fall states.
        FallFront,
        FallBack,
        FallLeft,
        FallRight,

        // Attack Melee states.
        AttackMeleeFront,
        AttackMeleeBack,
        AttackMeleeLeft,
        AttackMeleeRight,

        // Attack Ranged states.
        AttackRangedFront,
        AttackRangedBack,
        AttackRangedLeft,
        AttackRangedRight,

        // Block states.
        BlockFront,
        BlockBack,
        BlockLeft,
        BlockRight,

        // Take Damage states.
        DamageFront,
        DamageBack,
        DamageLeft,
        DamageRight,

        // Die states.
        DieFront,
        DieBack,
        DieLeft,
        DieRight
    }

    SpriteState spriteState = SpriteState.IdleFront;


    public NPCActor(GameController gameController, String playerName, Texture texture, Enums.Direction  direction, Level level) {
        super(gameController, playerName, texture, direction, level);
    }


    public void render(float delta, SpriteBatch batch) {
        super.render(delta);

         /*
         * All animation numbering in TextureRegion[AnimationType][AnimationFrames].
         *
         * // Idle -
         * 0 = IdleBack.
         * 1 = IdleFront.
         * 2 = IdleLeft.
         * 3 = IdleRight. (Idle right will just be the flipped version of Idle left)
         *
         * // Moving -
         * 4 = MoveUp.
         * 5 = MoveDown.
         * 6 = MoveLeft.
         * 7 = MoveRight. (Move right will just be the flipped version of Move left)
         *
         * // Turning -
         * 8 = TurnLeftFromFront.
         * 9 = TurnRightFromFront
         * 10 = TurnLeftFromBack.
         * 11 = TurnRightFromBack.
         * 12 = TurnLeftFromLeft. (Turn Left from Left will just be the flipped version of Turn Right from Front)
         * 13 = TurnRightFromLeft. (Turn Right from Left will just be the flipped version of Turn Left from Back)
         * 14 = TurnLeftFromRight. (Turn Left from Right will just be the flipped version of Turn Right from Back)
         * 15 = TurnRightFromRight. (Turn Right from Right will just be the flipped version of Turn Left from Front)
         *
         * // Falling -
         * 16 = FallFront.
         * 17 = FallBack.
         * 18 = FallLeft.
         * 19 = FallRight. (Fall Right will just be the flipped version of Fall Left)
         *
         * // Melee -
         * 20 = AttackMelee1Front.
         * 21 = AttackMelee2Front.
         * 22 = AttackMelee1Back.
         * 23 = AttackMelee2Back.
         * 24 = AttackMelee1Left.
         * 25 = AttackMelee2Left.
         * 26 = AttackMelee1Right. (Attack melee right 1 will just be the flipped version of Attack melee left 1)
         * 27 = AttackMelee2Right. (Attack melee right 2 will just be the flipped version of Attack melee left 2)
         *
         * // Ranged -
         * 28 = AttackRanged1Front.
         * 29 = AttackRanged2Front.
         * 30 = AttackRanged1Back.
         * 31 = AttackRanged2Back.
         * 32 = AttackRanged1Left.
         * 33 = AttackRanged2Left.
         * 34 = AttackRanged1Right. (Attack ranged right 1 will just be the flipped version of Attack ranged left 1)
         * 35 = AttackRanged2Right. (Attack ranged right 2 will just be the flipped version of Attack ranged left 2)
         *
         * // Block -
         * 36 = BlockFront.
         * 37 = BlockBack.
         * 38 = BlockLeft.
         * 39 = BlockRight. (Block Right will just be the flipped version of Block left)
         *
         * // Take Damage -
         * 40 = DamagedFront.
         * 41 = DamagedBack.
         * 42 = DamagedLeft.
         * 43 = DamagedRight. (Damaged Right will just be the flipped version of DamagedLeft)
         *
         * // Die -
         * 44 = DieFront.
         * 45 = DieBack.
         * 46 = DieLeft.
         * 47 = DieRight. (Die Right will just be the flipped version of DieLeft)
         */

        Enums.SpriteDirection spriteDirection = getSpriteDirection();


        // Idle.
        if (performingActions() == false || gameController.player.isDead) {
            if (spriteDirection == Enums.SpriteDirection.UP) {
                // Play Idle Back.
                currentSpriteIndex = 0;
            } else if (spriteDirection == Enums.SpriteDirection.DOWN) {
                // Play Idle Front.
                currentSpriteIndex = 1;
            } else if (spriteDirection == Enums.SpriteDirection.LEFT) {
                // Play Idle Left.
                currentSpriteIndex = 2;
            } else {
                // Play Idle Right.
                currentSpriteIndex = 3;
            }
            animationSpeed = 0.25f;
        }
        // Move.
        else if (isMoving) {
            if (spriteDirection == Enums.SpriteDirection.UP) {
                // Play Move UP.
                currentSpriteIndex = 0;

            } else if (spriteDirection == Enums.SpriteDirection.DOWN) {
                // Play Move DOWN.
                currentSpriteIndex = 1;

            } else if (spriteDirection == Enums.SpriteDirection.LEFT) {
                // Play Move Left.
                currentSpriteIndex = 2;

            } else {
                // Play Move Right.
                currentSpriteIndex = 3;
            }
            animationSpeed = (float)movementSpeed;
        }
        // Turn Left.
        else if (isRotatingLeft) {
            if (spriteDirection == Enums.SpriteDirection.UP) {
                // Play Turn Left from Back.

            } else if (spriteDirection == Enums.SpriteDirection.DOWN) {
                // Play Turn Left from Front.

            } else if (spriteDirection == Enums.SpriteDirection.LEFT) {
                // Play Turn Left from Left.

            } else {
                // Play Turn Left from Right.
            }
            animationSpeed = (float)rotationSpeed;
        }
        // Turn Right.
        else if (isRotatingRight) {
            if (spriteDirection == Enums.SpriteDirection.UP) {
                // Play Turn Right from Back.

            } else if (spriteDirection == Enums.SpriteDirection.DOWN) {
                // Play Turn Right from Front.

            } else if (spriteDirection == Enums.SpriteDirection.LEFT) {
                // Play Turn Right from Left.

            } else {
                // Play Turn Right from Right.

            }
            animationSpeed = (float)rotationSpeed;
        }
        // Melee Attack.
        else if (isAttacking && attackMode == attackMode.MELEE) {
            animationSpeed = (float) attackSpeed;
            if (spriteDirection == Enums.SpriteDirection.UP) {
                // Play Idle Back.
                currentSpriteIndex = 0;
            } else if (spriteDirection == Enums.SpriteDirection.DOWN) {
                // Play Idle Front.
                currentSpriteIndex = 1;
            } else if (spriteDirection == Enums.SpriteDirection.LEFT) {
                // Play Idle Left.
                currentSpriteIndex = 2;
            } else {
                // Play Idle Right.
                currentSpriteIndex = 3;
            }
            animationSpeed = (float)attackSpeed;
                /*
            if (spriteDirection == Enums.SpriteDirection.UP) {
                // Play Melee Attack Back.

            } else if (spriteDirection == Enums.SpriteDirection.DOWN) {
                // Play Melee Attack Front.

            } else if (spriteDirection == Enums.SpriteDirection.LEFT) {
                // Play Melee Attack Left.

            } else {
                // Play Melee Attack Right.

            }*/
        }
        // Ranged Attack.
        else if (isAttacking && attackMode == attackMode.RANGED) {
            if (spriteDirection == Enums.SpriteDirection.UP) {
                // Play Ranged Attack Back.

            } else if (spriteDirection == Enums.SpriteDirection.DOWN) {
                // Play Ranged Attack Front.

            } else if (spriteDirection == Enums.SpriteDirection.LEFT) {
                // Play Ranged Attack Left.

            } else {
                // Play Ranged Attack Right.

            }
            animationSpeed = (float)attackSpeed;
        }
        // Block.  // Need to do block raise and block drop.
        else if (isBlocking) {
            if (spriteDirection == Enums.SpriteDirection.UP) {
                // Play Block Back.

            } else if (spriteDirection == Enums.SpriteDirection.DOWN) {
                // Play Block Front.

            } else if (spriteDirection == Enums.SpriteDirection.LEFT) {
                // Play Block Left.

            } else {
                // Play Block Right.

            }
            animationSpeed = (float)blockSpeed;
        }


        currentAnimationTime += delta;

        float currentFrame = currentAnimationTime % animationSpeed;
        float frameLength = animationSpeed / (animations[currentSpriteIndex].length);
        for (int i = 0; i < animations[currentSpriteIndex].length; i++) {
            if (currentFrame >= i * frameLength) {
                playerSprite = new Sprite(animations[currentSpriteIndex][i]);
            }
        }

        playerSprite.setSize(128, 128);

        playerSprite.setCenter(Position.x, Position.y);
        playerSprite.setRotation(-gameController.inGameCamera.rotation);

        playerSprite.draw(batch);

        if (isDead) {
            path = null;
        }
    }

    public Enums.SpriteDirection getSpriteDirection() {
        // Set sprite Direction.
        switch (gameController.player.Direction) {
            case NORTH:
                switch (Direction) {
                    case NORTH:
                        return Enums.SpriteDirection.UP;
                    case EAST:
                        return Enums.SpriteDirection.RIGHT;
                    case SOUTH:
                        return Enums.SpriteDirection.DOWN;
                    case WEST:
                        return Enums.SpriteDirection.LEFT;
                    default:
                        break;
                }
                break;
            case EAST:
                switch (Direction) {
                    case NORTH:
                        return Enums.SpriteDirection.LEFT;
                    case EAST:
                        return Enums.SpriteDirection.UP;
                    case SOUTH:
                        return Enums.SpriteDirection.RIGHT;
                    case WEST:
                        return Enums.SpriteDirection.DOWN;
                    default:
                        break;
                }
                break;
            case SOUTH:
                switch (Direction) {
                    case NORTH:
                        return Enums.SpriteDirection.DOWN;
                    case EAST:
                        return Enums.SpriteDirection.LEFT;
                    case SOUTH:
                        return Enums.SpriteDirection.UP;
                    case WEST:
                        return Enums.SpriteDirection.RIGHT;
                    default:
                        break;
                }
                break;
            case WEST:
                switch (Direction) {
                    case NORTH:
                        return Enums.SpriteDirection.RIGHT;
                    case EAST:
                        return Enums.SpriteDirection.DOWN;
                    case SOUTH:
                        return Enums.SpriteDirection.LEFT;
                    case WEST:
                        return Enums.SpriteDirection.UP;
                    default:
                        break;
                }
                break;
            default:
                break;
        }
        return null;
    }
}
