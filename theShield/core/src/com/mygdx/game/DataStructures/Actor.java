package com.mygdx.game.DataStructures;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by Solist on 26/03/2017.
 */

public abstract class Actor extends GameObject {

    public String actorName;

    public RectangleMapObject playerObject;

    public Vector2 Position = new Vector2();
    public Enums.Direction Direction;

    public ActorType type;

    // Animation.
    public Texture animationTexture;
    public TextureRegion[][] animations;
    public Sprite playerSprite;
    public Vector2 regionSize;
    public int currentSpriteIndex = 0;
    public Enums.SpriteDirection spriteDirection;
    public float currentAnimationTime = 0;
    public float animationSpeed = 1.0f;

    // Sounds.
    public Sound[][] playerSounds;


    public enum ActorType {
        NONE,
        WALL,
        FLOOR,
        TRAP,
        BUFF,
        PLAYER,
        ENEMY,
        PROJECTILE,
        DOOR,
        STAIR,
        EFFECT
    }


    public Actor(Texture texture, Vector2 regionSize) {
        animationTexture = texture;
        animations = TextureRegion.split(animationTexture, (int)regionSize.x, (int)regionSize.y);
        this.regionSize = regionSize;
    }


    public float getEffectVolume() {
        return gameController.effectsVolume * gameController.masterVolume;
    }

    // Setters.
    public void setPosition(Vector2 position) {
        Position = position;
    }

    public void setDirection(Enums.Direction direction) {
        Direction = direction;
    }

    public void setSpriteDirection(Enums.SpriteDirection direction) {
        spriteDirection = direction;
    }

    public void render(float delta) {
        // Update sprite Direction.
        switch (gameController.player.Direction) {
            case NORTH:
                switch (Direction) {
                    case NORTH:
                        break;
                    case EAST:
                        break;
                    case SOUTH:
                        break;
                    case WEST:
                        break;
                    default:
                        break;
                }
                break;
            case EAST:
                switch (Direction) {
                    case NORTH:
                        break;
                    case EAST:
                        break;
                    case SOUTH:
                        break;
                    case WEST:
                        break;
                    default:
                        break;
                }
                break;
            case SOUTH:
                switch (Direction) {
                    case NORTH:
                        break;
                    case EAST:
                        break;
                    case SOUTH:
                        break;
                    case WEST:
                        break;
                    default:
                        break;
                }
                break;
            case WEST:
                switch (Direction) {
                    case NORTH:
                        break;
                    case EAST:
                        break;
                    case SOUTH:
                        break;
                    case WEST:
                        break;
                    default:
                        break;
                }
                break;
            default:
                break;
        }
    }
}
