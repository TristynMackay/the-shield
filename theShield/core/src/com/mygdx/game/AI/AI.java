package com.mygdx.game.AI;

import com.badlogic.gdx.math.Vector2;
import com.mygdx.game.Characters.Creature;
import com.mygdx.game.Characters.Player;
import com.mygdx.game.Controllers.GameController;
import com.mygdx.game.DataStructures.Enums;
import com.mygdx.game.DataStructures.GameObject;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Ying on 25/03/2017.
 */

public class AI extends GameObject {
    public AI(GameController gameController){
        this.gameController = gameController;
    }

    public int calH(Location loc, Player player){
        int total = 0;
        int cellX = 0;
        int cellY = 0;
        cellX = cellX - player.cellX;
        cellY = cellY - player.cellY;
        if(cellX < 0){
            cellX = -cellX;
        }
        if(cellY < 0){
            cellY = -cellY;
        }
        total = cellX + cellY;
        return total;
    }
    public int calG(Location loc, Creature creature){
        int total = 0;
        int cellX = 0;
        int cellY = 0;
        cellX = cellX - creature.cellX;
        cellY = cellY - creature.cellY;
        if(cellX < 0){
            cellX = -cellX;
        }
        if(cellY < 0){
            cellY = -cellY;
        }
        total = cellX + cellY;
        return total;
    }

    public int calF(Location loc , Player player, Creature creature){
        int total = this.calG(loc, creature) + this.calH(loc, player);
        return total;
    }

    public Iterable<Location> getNeighbours(Location loc) {
        ArrayList<Location> neighbours = new ArrayList<Location>();
        if (gameController.level.getWallCell(loc.getCellX(), loc.getCellY() - 1) == null) {
            neighbours.add(new Location(loc.getCellX(), loc.getCellY() - 1));
        }
        if (gameController.level.getWallCell(loc.getCellX(), loc.getCellY() + 1) == null) {
            neighbours.add(new Location(loc.getCellX(), loc.getCellY() + 1));
        }
        if (gameController.level.getWallCell(loc.getCellX() - 1, loc.getCellY()) == null) {
            neighbours.add(new Location(loc.getCellX() - 1, loc.getCellY()));
        }
        if (gameController.level.getWallCell(loc.getCellX() + 1, loc.getCellY()) == null) {
            neighbours.add(new Location(loc.getCellX() + 1, loc.getCellY()));
        }
        return neighbours;
    }

    public boolean detectRange(Player player, Creature creature, int range){
        ArrayList<Location> rangeList = new ArrayList<Location>();
        Location current = new Location(creature.cellX, creature.cellY);
        Location goal = new Location(player.cellX, player.cellY);
        Location tmpLocation = new Location(creature.cellX - range, creature.cellY + range);
        int height = range *2 + 1;
        int width = range * 2 + 1;
        int totalAvaliableCell = height & width - 1;
        for(int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                if (gameController.collisions.booleanWallCollision(tmpLocation.getCellX() + i, tmpLocation.getCellY() - j).equals(false) &&
                        gameController.collisions.checkStairCollision(tmpLocation.getCellX() + i, tmpLocation.getCellY() - j).equals(false)) {
                    rangeList.add(new Location(tmpLocation.getCellX() + i, tmpLocation.getCellY() - j));
                }
            }
        }
        rangeList.remove(current);
        for(Location i : rangeList){
            if(goal.equals(i)){
                return true;
            }
        }
        return  false;
    }
    public void performAction(Creature creature , ArrayList<Location> path, float delta) {
        if (creature.performingActions() == false) {
            Location moveLocation = new Location(0, 0);
            if (detectRange(gameController.player, creature, creature.detectRange) == true) {
                if (checkPlayerInAttackRange(gameController.player, creature, creature.attackRange)) {
                    if (gameController.player.lives > 0) {
                        if (creature.Direction == Enums.Direction.NORTH) {
                            if (creature.cellX < gameController.player.cellX && creature.cellY == gameController.player.cellY) {
                                creature.setStartTime(delta);
                                creature.isRotatingRight = true;
                                creature.TurnRight(delta);
                            } else if (creature.cellX > gameController.player.cellX && creature.cellY == gameController.player.cellY) {
                                creature.setStartTime(delta);
                                creature.isRotatingLeft = true;
                                creature.TurnLeft(delta);
                            }
                        } else if (creature.Direction == Enums.Direction.EAST) {
                            if (creature.cellX == gameController.player.cellX && creature.cellY > gameController.player.cellY) {
                                creature.setStartTime(delta);
                                creature.isRotatingRight = true;
                                creature.TurnRight(delta);
                            } else if (creature.cellX == gameController.player.cellX && creature.cellY < gameController.player.cellY) {
                                creature.setStartTime(delta);
                                creature.isRotatingLeft = true;
                                creature.TurnLeft(delta);
                            }
                        } else if (creature.Direction == Enums.Direction.SOUTH) {
                            if (creature.cellX > gameController.player.cellX && creature.cellY == gameController.player.cellY) {
                                creature.setStartTime(delta);
                                creature.isRotatingRight = true;
                                creature.TurnRight(delta);
                            } else if (creature.cellX < gameController.player.cellX && creature.cellY == gameController.player.cellY) {
                                creature.setStartTime(delta);
                                creature.isRotatingLeft = true;
                                creature.TurnLeft(delta);
                            }
                        } else if (creature.Direction == Enums.Direction.WEST) {
                            if (creature.cellX == gameController.player.cellX && creature.cellY < gameController.player.cellY) {
                                creature.setStartTime(delta);
                                creature.isRotatingRight = true;
                                creature.TurnRight(delta);
                            } else if (creature.cellX == gameController.player.cellX && creature.cellY > gameController.player.cellY) {
                                creature.setStartTime(delta);
                                creature.isRotatingLeft = true;
                                creature.TurnLeft(delta);
                            }
                        }
                        if (creature.attackMode == Enums.AttackType.MELEE) {
                            creature.MeleeAttack(delta);
                        }
                        else {
                            creature.RangedAttack(delta);
                        }
                        path = null;
                    }
                } else {
                    creature.path = findPath(gameController.player, creature, true);
                }
            }

            if (path != null) {
                if (path.size() > 0) {
                    moveLocation = path.get(0);
                    if (creature.cellX - 1 == moveLocation.getCellX() && creature.cellY == moveLocation.getCellY()) {
                        if (creature.Direction == Enums.Direction.WEST &&
                                gameController.level.positionsFlaggedForMoving[creature.cellX - 1][creature.cellY] == false) {
                            if (canMove(creature.cellX - 1, creature.cellY) == true) {
                                gameController.level.positionsFlaggedForMoving[creature.cellX - 1][creature.cellY] = true;
                                creature.cellX = moveLocation.getCellX();
                                creature.cellY = moveLocation.getCellY();
                                creature.setStartTime(delta);
                                creature.setStartingPosition();
                                creature.isMoving = true;
                                creature.MoveForward(delta);
                                creature.path.remove(0);
                            }
                        } else if (creature.Direction == Enums.Direction.NORTH) {
                            creature.setStartTime(delta);
                            creature.isRotatingLeft = true;
                            creature.TurnLeft(delta);
                        } else if (creature.Direction == Enums.Direction.SOUTH) {
                            creature.setStartTime(delta);
                            creature.isRotatingRight = true;
                            creature.TurnRight(delta);
                        } else if (creature.Direction == Enums.Direction.EAST) {
                            Random rn = new Random();
                            int ran = rn.nextInt(1);
                            if (ran == 0) {
                                creature.setStartTime(delta);
                                creature.isRotatingRight = true;
                                creature.TurnRight(delta);
                            } else if (ran == 1) {
                                creature.setStartTime(delta);
                                creature.isRotatingLeft = true;
                                creature.TurnLeft(delta);
                            }
                        }

                    } else if (creature.cellX + 1 == moveLocation.getCellX() && creature.cellY == moveLocation.getCellY()) {
                        if (creature.Direction == Enums.Direction.EAST &&
                                gameController.level.positionsFlaggedForMoving[creature.cellX + 1][creature.cellY] == false) {
                            if (canMove(creature.cellX + 1, creature.cellY) == true) {
                                gameController.level.positionsFlaggedForMoving[creature.cellX + 1][creature.cellY] = true;
                                creature.cellX = moveLocation.getCellX();
                                creature.cellY = moveLocation.getCellY();
                                creature.setStartTime(delta);
                                creature.setStartingPosition();
                                creature.isMoving = true;
                                creature.MoveForward(delta);
                                creature.path.remove(0);
                            }
                        } else if (creature.Direction == Enums.Direction.NORTH) {
                            creature.setStartTime(delta);
                            creature.isRotatingRight = true;
                            creature.TurnRight(delta);
                        } else if (creature.Direction == Enums.Direction.SOUTH) {
                            creature.setStartTime(delta);
                            creature.isRotatingLeft = true;
                            creature.TurnLeft(delta);
                        } else if (creature.Direction == Enums.Direction.WEST) {
                            Random rn = new Random();
                            int ran = rn.nextInt(1);
                            if (ran == 0) {
                                creature.setStartTime(delta);
                                creature.isRotatingRight = true;
                                creature.TurnRight(delta);
                            } else if (ran == 1) {
                                creature.setStartTime(delta);
                                creature.isRotatingLeft = true;
                                creature.TurnLeft(delta);
                            }
                        }

                    } else if (creature.cellX == moveLocation.getCellX() && creature.cellY - 1 == moveLocation.getCellY()) {
                        if (creature.Direction == Enums.Direction.SOUTH &&
                                gameController.level.positionsFlaggedForMoving[creature.cellX][creature.cellY - 1] == false) {
                            if (canMove(creature.cellX, creature.cellY - 1) == true) {
                                gameController.level.positionsFlaggedForMoving[creature.cellX][creature.cellY - 1] = true;
                                creature.cellX = moveLocation.getCellX();
                                creature.cellY = moveLocation.getCellY();
                                creature.setStartTime(delta);
                                creature.setStartingPosition();
                                creature.isMoving = true;
                                creature.MoveForward(delta);
                                creature.path.remove(0);
                            }
                        } else if (creature.Direction == Enums.Direction.EAST) {
                            creature.setStartTime(delta);
                            creature.isRotatingRight = true;
                            creature.TurnRight(delta);
                        } else if (creature.Direction == Enums.Direction.WEST) {
                            creature.setStartTime(delta);
                            creature.isRotatingLeft = true;
                            creature.TurnLeft(delta);
                        } else if (creature.Direction == Enums.Direction.NORTH) {
                            Random rn = new Random();
                            int ran = rn.nextInt(1);
                            if (ran == 0) {
                                creature.setStartTime(delta);
                                creature.isRotatingRight = true;
                                creature.TurnRight(delta);
                            } else if (ran == 1) {
                                creature.setStartTime(delta);
                                creature.isRotatingLeft = true;
                                creature.TurnLeft(delta);
                            }
                        }
                    } else if (creature.cellX == moveLocation.getCellX() && creature.cellY + 1 == moveLocation.getCellY()) {
                        if (creature.Direction == Enums.Direction.NORTH &&
                                gameController.level.positionsFlaggedForMoving[creature.cellX][creature.cellY + 1] == false) {
                            if (canMove(creature.cellX, creature.cellY + 1) == true) {
                                gameController.level.positionsFlaggedForMoving[creature.cellX][creature.cellY + 1] = true;
                                creature.cellX = moveLocation.getCellX();
                                creature.cellY = moveLocation.getCellY();
                                creature.setStartTime(delta);
                                creature.setStartingPosition();
                                creature.isMoving = true;
                                creature.MoveForward(delta);
                                creature.path.remove(0);
                            }
                        } else if (creature.Direction == Enums.Direction.EAST) {
                            creature.setStartTime(delta);
                            creature.isRotatingLeft = true;
                            creature.TurnLeft(delta);
                        } else if (creature.Direction == Enums.Direction.WEST) {
                            creature.setStartTime(delta);
                            creature.isRotatingRight = true;
                            creature.TurnRight(delta);
                        } else if (creature.Direction == Enums.Direction.SOUTH) {
                            Random rn = new Random();
                            int ran = rn.nextInt(1);
                            if (ran == 0) {
                                creature.setStartTime(delta);
                                creature.isRotatingRight = true;
                                creature.TurnRight(delta);
                            } else if (ran == 1) {
                                creature.setStartTime(delta);
                                creature.isRotatingLeft = true;
                                creature.TurnLeft(delta);
                            }
                        }
                    }
                }
            }
        } else if (creature.isRotatingLeft) {
            creature.TurnLeft(delta);
        } else if (creature.isRotatingRight) {
            creature.TurnRight(delta);
        } else if (creature.isMoving) {
            creature.MoveForward(delta);
        } else if (creature.isAttacking) {
            if (creature.attackMode == Enums.AttackType.MELEE) {
                creature.MeleeAttack(delta);
            } else {
                creature.RangedAttack(delta);
            }
        }
    }


    public Boolean checkPlayerInAttackRange(Player player, Creature creature, int attackRange){
        for(int i = 1; i < attackRange + 1; i++) {
            if (player.cellX == creature.cellX + i && player.cellY == creature.cellY) {
                return true;
            }
            if (player.cellX == creature.cellX - i && player.cellY == creature.cellY) {
                return true;
            }
            if (player.cellX == creature.cellX && player.cellY == creature.cellY + i) {
                return true;
            }
            if (player.cellX == creature.cellX && player.cellY == creature.cellY - i) {
                return true;
            }
        }
        return false;
    }

    public ArrayList<Location> findPath(Player player, Creature creature, Boolean detectTraps) {
        ArrayList<Location> path = new ArrayList<Location>();
        ArrayList<Location> openList = new ArrayList<Location>();
        ArrayList<Location> closeList = new ArrayList<Location>();
        Location start = new Location(creature.cellX, creature.cellY);
        Location current = new Location(creature.cellX, creature.cellY);
        Location goal = new Location(player.cellX, player.cellY);
        openList.add(current);

        while (player.lives!= 0 && creature.lives != 0)  {
            //Searching the lowest totalCost
            for (int i = 0; i < openList.size(); i++) {
                for (int y = 1; y < openList.size(); y++) {
                    if (this.calF(openList.get(i), player, creature) > this.calF(openList.get(y), player, creature)) {
                        Location tmpLocation = openList.get(i);
                        openList.set(i, openList.get(y));
                        openList.set(y, tmpLocation);
                    }
                }
            }

            current = openList.get(0);
            openList.remove(current);
            closeList.add(current);

            //when it found the goal
            if (current.equals(goal)) {
                ArrayList<Location> reverse = new ArrayList<Location>();
                //tracking backward from the goal to the starting point
                while (!current.equals(start)) {
                    current = current.getParent();
                    reverse.add(0, current);
                }
                //Moving Creatures
                for (Location i : reverse) {
                    path.add(i);
                }
                path.remove(0);
                return path;

            }
            //finding the neighbours in the current location and set their parent,
            if (creature.detectTraps == false) {
                for (Location i : getNeighbours(current)) {
                    if (closeList.contains(i)) {
                        continue;
                    } else if (!openList.contains(i) &&
                            gameController.level.getWallCell(i.getCellX(), i.getCellY()) == null &&
                            gameController.level.getStairCell(i.getCellX(), i.getCellY()) == null) {
                        i.setParent(current);
                        openList.add(i);
                    }
                }
            } else if (creature.detectTraps == true) {
                for (Location i : getNeighbours(current)) {
                    if (closeList.contains(i)) {
                        continue;

                    } else if (!openList.contains(i) &&
                            gameController.collisions.checkTrapCollision(new Vector2(i.getCellX() * 128 + 64, i.getCellY() * 128 + 64)) == null) {
                        i.setParent(current);
                        openList.add(i);
                    }
                }
            }
        }
        return path;
    }


    public boolean canMove(int cellX, int cellY) {
        Vector2 position = new Vector2(cellX * 128 + 64, cellY * 128 + 64);

        if (gameController.collisions.checkPlayerCollision(position) != null) {
            return false;
        }
        if (gameController.collisions.checkEnemyCollision(position) != null) {
            return false;
        }
        return true;
    }
}
