package com.mygdx.game.AI;

/**
 * Created by Ying on 25/4/2017.
 */

public class Location {
    private int cellX, cellY;
    private Location parent;

    public Location( int cellX, int cellY) {
        this.cellY = cellY;
        this.cellX = cellX;
    }
    public void setParent(Location parent){
        this.parent = parent;
    }

    public Location getParent() {
        return this.parent;
    }

    public int getCellY() {
        return cellY;
    }

    public int getCellX() {
        return cellX;
    }

    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + cellX;
        result = prime * result + cellY;
        return result;
    }

    public boolean equals(Object obj) {
        if (obj == null || getClass() != obj.getClass()) return false;
        Location other = (Location) obj;
        return this == obj || other.cellX == cellX && other.cellY == cellY;
    }

}
