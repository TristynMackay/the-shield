package com.mygdx.game.Traps;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.game.Controllers.GameController;
import com.mygdx.game.DataStructures.Actor;
import com.mygdx.game.DataStructures.PlayingActor;
import com.mygdx.game.Levels.Level;

/**
 * Created by Solist on 25/03/2017.
 */

public abstract class Trap extends Actor {

    public enum TrapType {
        PITTRAP,
        SPIKETRAP
    }

    public int damage;
    public Boolean used = false;

    public int cellX;
    public int cellY;


    TrapType trapType;

    public Trap(GameController gameController, TrapType trapType, String trapName,
                Texture texture, Boolean used, Level level) {
        super(texture, new Vector2(128, 128));

        this.gameController = gameController;

        playerObject = (RectangleMapObject)level.trapLayer.getObjects().get(trapName);
        playerObject.getRectangle().setCenter(playerObject.getRectangle().x + 128, playerObject.getRectangle().y + 128);
        Position.x = playerObject.getRectangle().x;
        Position.y = playerObject.getRectangle().y;

        this.trapType = trapType;

        type = ActorType.TRAP;

        this.used = used;

        Direction = Direction.NORTH;
    }

    public void useTrap(PlayingActor actor){
        if (damage == -1) {
            actor.ReceiveDamage(actor.totalLives, this);
        }
        else {
            actor.ReceiveDamage(damage, this);
        }
    }


    public boolean render(float delta, SpriteBatch batch) {
        super.render(delta);

        if (used == false) {
            playerSprite = new Sprite(animations[currentSpriteIndex][0]); // Need to change the secondary field of animations to accommodate for sprite timing.
            playerSprite.setSize(128, 128);

            playerSprite.setCenter(Position.x, Position.y);

            switch (gameController.inGameCamera.Direction) {
                case NORTH:
                    playerSprite.setRotation(0);
                    break;
                case EAST:
                    playerSprite.setRotation(270);
                    break;
                case SOUTH:
                    playerSprite.setRotation(180);
                    break;
                case WEST:
                    playerSprite.setRotation(90);
                    break;
                default:
                    break;
            }

            if (gameController.inGameCamera.rotation == 0) {

            }

            playerSprite.draw(batch);

            return true;
        }
        else {
            // Play use sound.
            /*
            float effectVolume = getEffectVolume();
            long useSoundID = playerSounds[0].play();
            playerSounds[0].setVolume(useSoundID, effectVolume);
            */

            // Play activation animation.

            // Check current time against depletion time.
            return false;
        }
    }
}
