package com.mygdx.game.Traps;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.mygdx.game.Controllers.GameController;
import com.mygdx.game.DataStructures.Enums;
import com.mygdx.game.Levels.Level;

/**
 * Created by Solist on 28/05/2017.
 */

public class Trap_Pit extends Trap {

    public Trap_Pit(GameController gameController, int trapNumber,
                    Level level, int cellX, int cellY, Boolean used) {
        super(gameController, TrapType.PITTRAP, "Trap_Pit" + trapNumber,
                new Texture(Gdx.files.internal("level/trap/Trap_Pit.png")), used, level);

        actorName = "Pit Trap";

        this.cellX = cellX;
        this.cellY = cellY;

        damage = -1;
    }
}
