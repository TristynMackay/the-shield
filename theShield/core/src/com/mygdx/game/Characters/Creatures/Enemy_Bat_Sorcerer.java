package com.mygdx.game.Characters.Creatures;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.game.AI.Location;
import com.mygdx.game.Characters.Creature;
import com.mygdx.game.Controllers.GameController;
import com.mygdx.game.DataStructures.Enums;
import com.mygdx.game.Levels.Level;

import java.util.ArrayList;

/**
 * Created by Solist on 26/05/2017.
 */

public class Enemy_Bat_Sorcerer extends Creature {


    public Enemy_Bat_Sorcerer(GameController gameController, Enums.Direction direction, int creatureNumber,
                     Level level, int cellX, int cellY) {
        super(gameController, "Enemy_Bat_Sorcerer" + creatureNumber,
                new Texture(Gdx.files.internal("sprite/character/bat/Red_Bat_SpriteSheet.png")), direction, level);

        actorName = "Sorcerer Bat";

        type = ActorType.ENEMY;
        lives = 1;
        totalLives = 1;
        baseTotalLives = totalLives;

        attackDamage = 2;
        baseAttackDamage = attackDamage;
        attackSpeed = 2.0;
        baseAttackSpeed = attackSpeed;
        attackRange = 3;
        baseAttackRange = attackRange;
        attackMode = Enums.AttackType.RANGED;

        projectileType = "FireBall";

        this.cellX = cellX;
        this.cellY = cellY;

        playerSounds = new Sound[5][];
        // Setup Grunt sounds.
        playerSounds[0] = new Sound[1];
        for (int i = 0; i < playerSounds[0].length; i++) {
            playerSounds[0][i] = Gdx.audio.newSound(
                    Gdx.files.internal("sound/character/bat/ATTACK_Screech" + (i + 1) + ".wav"));
        }
        // Setup receive Hit sounds.
        playerSounds[1] = new Sound[1];
        for (int i = 0; i < playerSounds[1].length; i++) {
            playerSounds[1][i] = Gdx.audio.newSound(
                    Gdx.files.internal("sound/character/hit/Hit_Fleshy.wav"));
        }
        // Setup Hurt sounds.
        playerSounds[2] = new Sound[1];
        for (int i = 0; i < playerSounds[2].length; i++) {
            playerSounds[2][i] = Gdx.audio.newSound(
                    Gdx.files.internal("sound/character/bat/HURT_Screech" + (i + 1) + ".wav"));
        }
        // Setup Death sounds.
        playerSounds[3] = new Sound[2];
        for (int i = 0; i < playerSounds[3].length; i++) {
            playerSounds[3][i] = Gdx.audio.newSound(
                    Gdx.files.internal("sound/character/bat/DYING_Screech" + (i + 1) + ".wav"));
        }
        // Setup Movement sounds.


        weaponSounds = new Sound[3];
        weaponSounds[0] = Gdx.audio.newSound(Gdx.files.internal("sound/character/bat/ATTACK_Swing.wav"));
        weaponSounds[2] = Gdx.audio.newSound(Gdx.files.internal("sound/weapon/Shoot_FireBall.wav"));

        currentSpriteIndex = 0;

        detectRange = 4;
        detectTraps = false;
        path = new ArrayList<Location>();
    }

    public void render(float delta, SpriteBatch batch, Vector2 position) {
        super.render(delta, batch, position);
        if (isDead == false) {
            gameController.ai.performAction(this, this.path, delta);
        }
    }
}
