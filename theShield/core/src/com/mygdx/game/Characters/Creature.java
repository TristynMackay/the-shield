package com.mygdx.game.Characters;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.game.Controllers.GameController;
import com.mygdx.game.DataStructures.Enums;
import com.mygdx.game.DataStructures.NPCActor;
import com.mygdx.game.Levels.Level;

/**
 * Created by Solist on 25/03/2017.
 */

public abstract class Creature extends NPCActor {

    public Creature(GameController gameController, String creatureName, Texture texture, Enums.Direction direction, Level level) {
        super(gameController, creatureName, texture, direction, level);

        type = ActorType.ENEMY;
    }


    public void render(float delta, SpriteBatch batch, Vector2 position) {
        super.render(delta, batch);
    }
}
