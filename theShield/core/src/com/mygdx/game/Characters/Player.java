package com.mygdx.game.Characters;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.mygdx.game.Blessings.Blessing;
import com.mygdx.game.Controllers.GameController;
import com.mygdx.game.DataStructures.Actor;
import com.mygdx.game.DataStructures.Enums;
import com.mygdx.game.DataStructures.PlayingActor;
import com.mygdx.game.Levels.Level;
import com.mygdx.game.Projectiles.Arrow;

import java.util.Random;

/**
 * Created by Solist on 26/03/2017.
 */

public class Player extends PlayingActor {

    public String playerName;

    public Player(GameController gameController, String playerName, Enums.Direction direction, Level level) {
        super(gameController, "Player", new Texture(Gdx.files.internal("level/Player_Male.png")), direction, level);

        this.playerName = playerName;

        actorName = "Player"; // Need to change this for character customisation.

        type = ActorType.PLAYER;
        lives = 2;
        totalLives = 2;
        baseTotalLives = totalLives;

        attackDamage = 1;
        baseAttackDamage = attackDamage;
        attackSpeed = 0.75;
        baseAttackSpeed = attackSpeed;
        blockSpeed = 0.25;
        baseBlockSpeed = blockSpeed;

        projectileType = "Arrow";

        //
        // Setup the player sounds.
        //
        playerSounds = new Sound [5][];
        // Setup male grunts.
        playerSounds[0] = new Sound[2];
        for (int i = 0; i < playerSounds[0].length; i++) {
            playerSounds[0][i] = Gdx.audio.newSound(
                    Gdx.files.internal("sound/character/player/male/GRUNT_Male" + (i + 1) + ".wav"));
        }
        // Setup male receive Hit sounds.
        playerSounds[1] = new Sound[1];
        for (int i = 0; i < playerSounds[1].length; i++) {
            playerSounds[1][i] = Gdx.audio.newSound(
                    Gdx.files.internal("sound/character/hit/Hit_Fleshy.wav"));
        }
        // Setup male Hurt sounds.
        playerSounds[2] = new Sound[4];
        for (int i = 0; i < playerSounds[2].length; i++) {
            playerSounds[2][i] = Gdx.audio.newSound(
                    Gdx.files.internal("sound/character/player/male/HURT_Male" + (i + 1) + ".wav"));
        }
        // Setup male Death sounds.
        playerSounds[3] = new Sound[2];
        for (int i = 0; i < playerSounds[3].length; i++) {
            playerSounds[3][i] = Gdx.audio.newSound(
                    Gdx.files.internal("sound/character/player/male/DYING_Male" + (i + 1) + ".wav"));
        }
        // Setup movement sounds.
        playerSounds[4] = new Sound[4];
        for (int i = 0; i < playerSounds[4].length; i++) {
            playerSounds[4][i] = Gdx.audio.newSound(
                    Gdx.files.internal("sound/character/player/WALKING" + (i + 1) + ".wav"));
        }

        //
        // Setup the weapon sounds.
        //
        weaponSounds = new Sound [6];
        weaponSounds[0] = Gdx.audio.newSound(Gdx.files.internal("sound/weapon/Swing_Miss_Single.wav"));
        weaponSounds[1] = Gdx.audio.newSound(Gdx.files.internal("sound/weapon/Block_Shield.wav"));
        weaponSounds[2] = Gdx.audio.newSound(Gdx.files.internal("sound/weapon/Shoot_Bow.wav"));
        weaponSounds[3] = Gdx.audio.newSound(Gdx.files.internal("sound/weapon/Sheath_Sword.wav"));
        weaponSounds[4] = Gdx.audio.newSound(Gdx.files.internal("sound/weapon/Unsheath_Sword.wav"));
        weaponSounds[5] = Gdx.audio.newSound(Gdx.files.internal("sound/weapon/Sheath_Bow.wav"));

        currentSpriteIndex = 0;


        //
        // Finish setting up the movement sounds.
        //
        if (playerSounds[4] != null) {
            movementSoundIntervals = new boolean[playerSounds[4].length];
        }
    }


    public Player(Player player, Enums.Direction direction, Level level) {
        super(player.gameController, "Player", new Texture(Gdx.files.internal("level/Player_Male.png")), direction, level);

        playerName = player.playerName;

        actorName = player.actorName; // Need to change this for character customisation.

        type = ActorType.PLAYER;
        lives = player.lives;
        totalLives = player.totalLives;
        baseTotalLives = player.baseTotalLives;

        creaturesKilled = player.creaturesKilled;

        attackMode = player.attackMode;

        blessings = player.blessings;
        currentBlessings = player.currentBlessings;
        maxBlessings = player.maxBlessings;

        baseMovementSpeed = player.baseMovementSpeed;
        movementSpeed = player.movementSpeed;
        baseRotationSpeed = player.baseRotationSpeed;
        rotationSpeed = player.rotationSpeed;

        baseAttackDamage = player.baseAttackDamage;
        attackDamage = player.attackDamage;
        baseAttackSpeed = player.baseAttackSpeed;
        attackSpeed = player.attackSpeed;
        baseAttackRange = player.baseAttackRange;
        attackRange = player.attackRange;

        baseBlockSpeed = player.baseBlockSpeed;
        blockSpeed = player.blockSpeed;

        projectileType = player.projectileType;

        //
        // Setup the player sounds.
        //
        playerSounds = player.playerSounds;

        //
        // Setup the weapon sounds.
        //
        weaponSounds = player.weaponSounds;

        movementSoundIntervals = player.movementSoundIntervals;

        currentSpriteIndex = player.currentSpriteIndex;

        updateBuffs();
    }


    public Player(GameController gameController, String actorName, ActorType type,
                  int lives, int totalLives, int baseTotalLives,
                  int creaturesKilled, Enums.AttackType attackMode,
                  double baseMovementSpeed, double movementSpeed,
                  double baseRotationSpeed, double rotationSpeed,
                  int baseAttackDamage, int attackDamage,
                  double baseAttackSpeed, double attackSpeed,
                  int baseAttackRange, int attackRange,
                  double baseBlockSpeed, double blockSpeed,
                  Level level) {
        super(gameController, "Player", new Texture(Gdx.files.internal("level/Player_Male.png")), Enums.Direction.EAST, level);

        this.actorName = actorName; // Need to change this for character customisation.

        this.type = type;
        this.lives = lives;
        this.totalLives = totalLives;
        this.baseTotalLives = baseTotalLives;

        this.creaturesKilled = creaturesKilled;

        this.attackMode = attackMode;

        maxBlessings = 30;
        blessings = new Blessing[maxBlessings];
        currentBlessings = 0;

        this.baseMovementSpeed = baseMovementSpeed;
        this.movementSpeed = movementSpeed;
        this.baseRotationSpeed = baseRotationSpeed;
        this.rotationSpeed = rotationSpeed;

        this.baseAttackDamage = baseAttackDamage;
        this.attackDamage = attackDamage;
        this.baseAttackSpeed = baseAttackSpeed;
        this.attackSpeed = attackSpeed;
        this.baseAttackRange = baseAttackRange;
        this.attackRange = attackRange;

        this.baseBlockSpeed = baseBlockSpeed;
        this.blockSpeed = blockSpeed;

        //
        // Setup the player sounds.
        //
        playerSounds = new Sound [5][];
        // Setup male grunts.
        playerSounds[0] = new Sound[2];
        for (int i = 0; i < playerSounds[0].length; i++) {
            playerSounds[0][i] = Gdx.audio.newSound(
                    Gdx.files.internal("sound/character/player/male/GRUNT_Male" + (i + 1) + ".wav"));
        }
        // Setup male receive Hit sounds.
        playerSounds[1] = new Sound[1];
        for (int i = 0; i < playerSounds[1].length; i++) {
            playerSounds[1][i] = Gdx.audio.newSound(
                    Gdx.files.internal("sound/character/hit/Hit_Fleshy.wav"));
        }
        // Setup male Hurt sounds.
        playerSounds[2] = new Sound[4];
        for (int i = 0; i < playerSounds[2].length; i++) {
            playerSounds[2][i] = Gdx.audio.newSound(
                    Gdx.files.internal("sound/character/player/male/HURT_Male" + (i + 1) + ".wav"));
        }
        // Setup male Death sounds.
        playerSounds[3] = new Sound[2];
        for (int i = 0; i < playerSounds[3].length; i++) {
            playerSounds[3][i] = Gdx.audio.newSound(
                    Gdx.files.internal("sound/character/player/male/DYING_Male" + (i + 1) + ".wav"));
        }
        // Setup movement sounds.
        playerSounds[4] = new Sound[4];
        for (int i = 0; i < playerSounds[4].length; i++) {
            playerSounds[4][i] = Gdx.audio.newSound(
                    Gdx.files.internal("sound/character/player/WALKING" + (i + 1) + ".wav"));
        }

        //
        // Setup the weapon sounds.
        //
        weaponSounds = new Sound [6];
        weaponSounds[0] = Gdx.audio.newSound(Gdx.files.internal("sound/weapon/Swing_Miss_Single.wav"));
        weaponSounds[1] = Gdx.audio.newSound(Gdx.files.internal("sound/weapon/Block_Shield.wav"));
        weaponSounds[2] = Gdx.audio.newSound(Gdx.files.internal("sound/weapon/Shoot_Bow.wav"));
        weaponSounds[3] = Gdx.audio.newSound(Gdx.files.internal("sound/weapon/Sheath_Sword.wav"));
        weaponSounds[4] = Gdx.audio.newSound(Gdx.files.internal("sound/weapon/Unsheath_Sword.wav"));
        weaponSounds[5] = Gdx.audio.newSound(Gdx.files.internal("sound/weapon/Sheath_Bow.wav"));

        currentSpriteIndex = 0;

        updateBuffs();
    }


    @Override
    public void ReceiveDamage(int damage, Actor attacker) {
        super.ReceiveDamage(damage, attacker);
        livesChanged = true;
        if (lives <= 0) {
            gameController.inGameScreen.deathText.setText(
                    gameController.inGameScreen.deathString + killedBy + "!");
            gameController.inGameScreen.uiRendered = false;
            gameController.inGameScreen.menuActive = true;
        }
    }


    @Override
    public void ReceiveHealing(int healing) {
        super.ReceiveHealing(healing);
        livesChanged = true;
    }


    public void render(float delta, SpriteBatch batch, Vector2 position) {
        super.render(delta);

        currentAnimationTime += delta;

        float currentFrame = currentAnimationTime % animationSpeed;
        float frameLength = animationSpeed / animations[currentSpriteIndex].length;
        for (int i = 0; i < animations[currentSpriteIndex].length; i++) {
            if (currentFrame >= i * frameLength) {
                playerSprite = new Sprite(animations[currentSpriteIndex][i]);
            }
        }

        if (isMoving && isDead == false) {
            // Play movement sounds.
            if (playerSounds[4] != null) {
                float soundIntervalLength = (float) movementSpeed;
                float effectVolume = getEffectVolume();
                for (int i = 0; i < 3; i++) {
                    if (currentAnimationTime >= i * soundIntervalLength) {
                        if (movementSoundIntervals[i] == false) {
                            movementSoundIntervals[i] = true;
                            Random rand = new Random();
                            int randInt = rand.nextInt(playerSounds[4].length);
                            long soundID = playerSounds[4][randInt].play();
                            playerSounds[4][randInt].setVolume(soundID, effectVolume);

                        }
                    }
                }
            }
        }

        playerSprite.setSize(128, 128);

        playerSprite.setCenter(position.x, position.y);
        playerSprite.setRotation(-gameController.inGameCamera.rotation);

        playerSprite.draw(batch);
    }
}
