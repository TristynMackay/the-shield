package com.mygdx.game.Controllers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.mygdx.game.Blessings.Blessing;
import com.mygdx.game.DataStructures.Actor;
import com.mygdx.game.DataStructures.Enums;
import com.mygdx.game.DataStructures.GameObject;
import com.mygdx.game.Traps.Trap;

/**
 * Created by Solist on 25/03/2017.
 */

// For information regarding accelerometers
// go here https://github.com/libgdx/libgdx/wiki/Accelerometer
// (Referenced 25/03/2017)

public class InputController extends GameObject implements InputProcessor {

    int orientation = Gdx.input.getRotation();


    public void CheckInputs() {
        switch (Gdx.app.getType()) {
            case Android:
                CheckAndroidInputs();
                break;
            case iOS:
                CheckIOSInputs();
                break;
            default:
                break;
        }
    }


    // !!!!!!!!!!!!!!!!!!!!!
    // TODO - Get the accelerometer inputs for android devices.
    // !!!!!!!!!!!!!!!!!!!!!
    private void CheckAndroidInputs() {
        if (gameController.inGame == true && gameController.player.performingActions() == false) {
            if (Gdx.input.getAccelerometerX() < 3 && Gdx.input.getAccelerometerZ() > 9) {
                gameController.inGameCamera.setStartingPosition();
                gameController.player.setStartingPosition();
                float delta = Gdx.graphics.getDeltaTime();
                gameController.inGameCamera.setStartTime(delta);
                gameController.player.setStartTime(delta);
                gameController.player.isMoving = true;
            } else if (Gdx.input.getAccelerometerX() < 8 && Gdx.input.getAccelerometerY() < -5) {
                switch (gameController.inGameCamera.Direction) {
                    case NORTH:
                        gameController.inGameCamera.Direction = Enums.Direction.WEST;
                        break;
                    case EAST:
                        gameController.inGameCamera.Direction = Enums.Direction.NORTH;
                        break;
                    case SOUTH:
                        gameController.inGameCamera.Direction = Enums.Direction.EAST;
                        break;
                    case WEST:
                        gameController.inGameCamera.Direction = Enums.Direction.SOUTH;
                        break;
                    default:
                        break;
                }
                gameController.inGameCamera.setStartTime(Gdx.graphics.getDeltaTime());
                gameController.player.isRotatingLeft = true;
                gameController.inGameCamera.directionChanged = false;
            } else if (Gdx.input.getAccelerometerX() > 8 && Gdx.input.getAccelerometerY() > 5) {
                switch (gameController.inGameCamera.Direction) {
                    case NORTH:
                        gameController.inGameCamera.Direction = Enums.Direction.EAST;
                        break;
                    case EAST:
                        gameController.inGameCamera.Direction = Enums.Direction.SOUTH;
                        break;
                    case SOUTH:
                        gameController.inGameCamera.Direction = Enums.Direction.WEST;
                        break;
                    case WEST:
                        gameController.inGameCamera.Direction = Enums.Direction.NORTH;
                        break;
                    default:
                        break;
                }
                gameController.inGameCamera.setStartTime(Gdx.graphics.getDeltaTime());
                gameController.player.isRotatingRight = true;
                gameController.inGameCamera.directionChanged = false;
            }
        }
    }

    private void CheckIOSInputs() {
        /*
        if (Gdx.input.getAccelerometerX() > 0) {
            gameController.inGameCamera.MoveForward();
        } else if (Gdx.input.getAccelerometerY() > 0) {
            gameController.inGameCamera.TurnLeft();
        } else if (Gdx.input.getAccelerometerZ() > 0) {
            gameController.inGameCamera.TurnRight();
        }*/
        // else if (attack button pressed) { }
        // else if (block button pressed) { }
        // else if (weaponSwap button pressed) { }
        // else if (menu button pressed) { }
    }


    public boolean keyDown (int keycode) {
        if (gameController.inGame == true && gameController.player.isDead == false &&
                gameController.player.performingActions() == false) {
            if (keycode == Input.Keys.W) {
                int cellX = (int)((gameController.player.Position.x - 64) / 128);
                int cellY = (int)((gameController.player.Position.y - 64) / 128);
                switch (gameController.player.Direction) {
                    case NORTH:
                        cellY += 1;
                        break;
                    case EAST:
                        cellX += 1;
                        break;
                    case SOUTH:
                        cellY -= 1;
                        break;
                    case WEST:
                        cellX -= 1;
                        break;
                    default:
                        break;
                }
                // TODO - Cleanup the collisions logic here.
                Vector2 targetPosition = new Vector2(64 + (cellX * 128), 64 + (cellY * 128));
                if (gameController.collisions.checkStairCollision(cellX, cellY) == true) {
                    for (int i = 0; i < gameController.level.stairList.length; i++) {
                        if (targetPosition.x == gameController.level.stairList[i].Position.x &&
                                targetPosition.y == gameController.level.stairList[i].Position.y) {
                            gameController.level.stairList[i].use(gameController.player);
                        }
                    }
                }
                if (gameController.collisions.booleanWallCollision(cellX, cellY) == false &&
                        gameController.collisions.checkEnemyCollision(targetPosition) == null &&
                        gameController.level.positionsFlaggedForMoving[cellX][cellY] == false) {
                    gameController.level.positionsFlaggedForMoving[cellX][cellY] = true;
                    gameController.inGameCamera.setStartingPosition();
                    gameController.player.setStartingPosition();
                    float delta = Gdx.graphics.getDeltaTime();
                    gameController.inGameCamera.setStartTime(delta);
                    gameController.player.setStartTime(delta);
                    gameController.player.isMoving = true;
                    gameController.player.movementSoundIntervals = new boolean[gameController.player.playerSounds[4].length];
                }
                Trap trapCollision = gameController.collisions.checkTrapCollision(new Vector2(cellX * 128 + 64, cellY * 128 + 64));
                if (trapCollision != null) {
                    trapCollision.useTrap(gameController.player);
                }
                Blessing buffCollision = gameController.collisions.checkBuffCollision(new Vector2(cellX * 128 + 64, cellY * 128 + 64));
                if (buffCollision != null) {
                    buffCollision.useBuff(gameController.player);
                }
            } else if (keycode == Input.Keys.A) {
                switch (gameController.inGameCamera.Direction) {
                    case NORTH:
                        gameController.inGameCamera.Direction = Enums.Direction.WEST;
                        break;
                    case EAST:
                        gameController.inGameCamera.Direction = Enums.Direction.NORTH;
                        break;
                    case SOUTH:
                        gameController.inGameCamera.Direction = Enums.Direction.EAST;
                        break;
                    case WEST:
                        gameController.inGameCamera.Direction = Enums.Direction.SOUTH;
                        break;
                    default:
                        break;
                }
                gameController.inGameCamera.setStartTime(Gdx.graphics.getDeltaTime());
                gameController.player.isRotatingLeft = true;
                gameController.inGameCamera.directionChanged = false;
            } else if (keycode == Input.Keys.D) {
                switch (gameController.inGameCamera.Direction) {
                    case NORTH:
                        gameController.inGameCamera.Direction = Enums.Direction.EAST;
                        break;
                    case EAST:
                        gameController.inGameCamera.Direction = Enums.Direction.SOUTH;
                        break;
                    case SOUTH:
                        gameController.inGameCamera.Direction = Enums.Direction.WEST;
                        break;
                    case WEST:
                        gameController.inGameCamera.Direction = Enums.Direction.NORTH;
                        break;
                    default:
                        break;
                }
                gameController.inGameCamera.setStartTime(Gdx.graphics.getDeltaTime());
                gameController.player.isRotatingRight = true;
                gameController.inGameCamera.directionChanged = false;
            } else if (keycode == Input.Keys.Q) {
                InputEvent buttonEvent = new InputEvent();
                buttonEvent.setType(InputEvent.Type.touchDown);
                if (gameController.player.attackMode == Enums.AttackType.MELEE) {
                    gameController.inGameScreen.swordButton.fire(buttonEvent);
                } else {
                    gameController.inGameScreen.bowButton.fire(buttonEvent);
                }
            } else if (keycode == Input.Keys.R) {
                if (gameController.player.isSwitchingWeapons == false) {
                    gameController.player.isSwitchingWeapons = true;
                    gameController.player.switchWeaponStartTime = 0;
                    gameController.player.switchWeaponCurrentTime = 0;
                    InputEvent buttonEvent = new InputEvent();
                    buttonEvent.setType(InputEvent.Type.touchDown);
                    if (gameController.player.attackMode == Enums.AttackType.MELEE) {
                        gameController.inGameScreen.swapBowButton.fire(buttonEvent);
                    } else {
                        gameController.inGameScreen.swapSwordButton.fire(buttonEvent);
                    }
                }
            }

            return true;
        }
        return false;
    }

    public boolean keyUp (int keycode) {
        InputEvent buttonEvent = new InputEvent();
        buttonEvent.setType(InputEvent.Type.touchUp);
        if (keycode == Input.Keys.Q) {
            if (gameController.player.attackMode == Enums.AttackType.MELEE) {
                gameController.inGameScreen.swordButton.fire(buttonEvent);
            } else {
                gameController.inGameScreen.bowButton.fire(buttonEvent);
            }
        } else if (keycode == Input.Keys.R) {
            if (gameController.player.attackMode == Enums.AttackType.MELEE) {
                gameController.inGameScreen.swapBowButton.fire(buttonEvent);
            } else {
                gameController.inGameScreen.swapSwordButton.fire(buttonEvent);
            }

            return true;
        }

        return false;
    }

    public boolean keyTyped (char character) {
        return false;
    }

    public boolean touchDown (int x, int y, int pointer, int button) {
        if (button == Input.Buttons.LEFT) {
            if (gameController.inGame == true) {
                if (gameController.inGameScreen.menuActive == false) {

                }
            }
        }
        return false;
    }

    public boolean touchUp (int x, int y, int pointer, int button) {
        return false;
    }

    public boolean touchDragged (int x, int y, int pointer) {
        return false;
    }

    public boolean mouseMoved (int x, int y) {
        return false;
    }

    public boolean scrolled (int amount) {
        return false;
    }
}
