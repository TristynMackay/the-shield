package com.mygdx.game.Controllers;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.game.DataStructures.Enums;
import com.mygdx.game.DataStructures.GameObject;

/**
 * Created by Solist on 25/03/2017.
 */

public class CameraController extends GameObject {

    // Variables.
    public OrthographicCamera camera;
    private float startTime = 0;
    private float currentTime = 0;
    private float previousTime = 0;
    public boolean directionChanged = false;
    public Vector2 Position = new Vector2();
    public Vector2 startPosition = new Vector2();

    public Enums.Direction Direction = Enums.Direction.NORTH;
    public int rotation;


    // Methods.

    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    // TODO - Implement the movement methods.
    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    // Movement.
    public void MoveForward(float delta) {
        // Smoothly move the camera forward 1 unit (square).
        currentTime += delta;
        if (currentTime - startTime <= gameController.player.movementSpeed) {
            switch (Direction) {
                case NORTH:
                    Position = new Vector2(startPosition.x, startPosition.y + (float)(128 * (currentTime - startTime)/ gameController.player.movementSpeed));
                    break;
                case EAST:
                    Position = new Vector2(startPosition.x  + (float)(128 * (currentTime - startTime)/ gameController.player.movementSpeed), startPosition.y);
                    break;
                case SOUTH:
                    Position = new Vector2(startPosition.x, startPosition.y - (float)(128 * (currentTime - startTime)/ gameController.player.movementSpeed));
                    break;
                case WEST:
                    Position = new Vector2(startPosition.x - (float)(128 * (currentTime - startTime)/ gameController.player.movementSpeed), startPosition.y);
                    break;
                default:
                    break;
            }
            camera.position.set(Position, 0);
            gameController.player.MoveForward(delta);
        }
        else if (gameController.player.isMoving == true){
            // fixPosition() method is used to update the position to the correct position as the
            // movement over time may not put us at the desired location.
            fixPosition();
            gameController.player.MoveForward(delta);
        }
        camera.update();
        // and move the character at the same time.
    }

    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    // TODO - Implement the rotation methods.
    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    // Rotation.
    public void TurnLeft(float delta) {
        // Smoothly rotate the camera 90 degrees to the left (-90 degrees).
        currentTime += delta;
        if (currentTime - startTime <= gameController.player.rotationSpeed) {
            int tempRotation = (int)-(90 * (currentTime - startTime - previousTime) /
                                        gameController.player.rotationSpeed);
            camera.rotate(tempRotation);
            gameController.inGameScreen.updateCompass(rotation);
            rotation += tempRotation;
            previousTime = currentTime - startTime;
            if (directionChanged == false &&
                    previousTime >= gameController.player.rotationSpeed / 2) {
                directionChanged = true;
                gameController.level.updateLevel(Direction);
            }
            gameController.player.TurnLeft(delta);
        }
        else if (gameController.player.isRotatingLeft == true) {
            gameController.player.TurnLeft(delta);
            fixCameraRotation();
            gameController.inGameScreen.populateUITable();
        }
        camera.update();
        // and rotate the character at the same time.

        // Change the camera's direction
        // (i.e. if the character's current direction is NORTH, change their Direction to WEST).
    }

    public void TurnRight(float delta) {
        // Smoothly rotate the camera 90 degrees to the right (90 degrees).
        currentTime += delta;
        if (currentTime - startTime <= gameController.player.rotationSpeed) {
            int tempRotation = (int)(90 * (currentTime - startTime - previousTime) /
                                        gameController.player.rotationSpeed);
            camera.rotate(tempRotation);
            gameController.inGameScreen.updateCompass(rotation);
            rotation += tempRotation;
            previousTime = currentTime - startTime;
            if (directionChanged == false &&
                    previousTime >= gameController.player.rotationSpeed / 2) {
                directionChanged = true;
                gameController.level.updateLevel(Direction);
            }
            gameController.player.TurnRight(delta);
        }
        else if (gameController.player.isRotatingRight == true) {
            gameController.player.TurnRight(delta);
            fixCameraRotation();
            gameController.inGameScreen.populateUITable();
        }
        camera.update();
        // and rotate the character at the same time.

        // Change the camera's direction
        // (i.e. if the character's current direction is NORTH, change their Direction to EAST).
    }


    public void startCamera() {
        switch (Direction) {
            case NORTH:
                rotation = 0;
                break;
            case EAST:
                rotation = 90;
                camera.rotate(90);
                break;
            case SOUTH:
                rotation = 180;
                camera.rotate(180);
                break;
            case WEST:
                rotation = 270;
                camera.rotate(270);
                break;
            default:
                break;
        }
        camera.update();
    }


    public void fixPosition() {
        startPosition.x = Math.round(startPosition.x);
        startPosition.y = Math.round(startPosition.y);
        switch (Direction){
            case NORTH:
                camera.position.set(startPosition.x, startPosition.y + 128, 0);
                Position.y = startPosition.y + 128;
                break;
            case EAST:
                camera.position.set(startPosition.x + 128, startPosition.y, 0);
                Position.x = startPosition.x + 128;
                break;
            case SOUTH:
                camera.position.set(startPosition.x, startPosition.y - 128, 0);
                Position.y = startPosition.y - 128;
                break;
            case WEST:
                camera.position.set(startPosition.x - 128, startPosition.y, 0);
                Position.x = startPosition.x - 128;
                break;
            default:
                break;
        }
    }


    public void fixCameraRotation() {
        if (rotation < 0) {
            rotation = 360 + rotation;
        }
        else if (rotation > 360) {
            rotation = 360;
        }

        switch (Direction) {
            case NORTH:
                camera.rotate(0 - rotation);
                gameController.inGameScreen.updateCompass(0);
                rotation = 0;
                break;
            case EAST:
                camera.rotate(90 - rotation);
                gameController.inGameScreen.updateCompass(90);
                rotation = 90;
                break;
            case SOUTH:
                camera.rotate(180 - rotation);
                gameController.inGameScreen.updateCompass(180);
                rotation = 180;
                break;
            case WEST:
                camera.rotate(270 - rotation);
                gameController.inGameScreen.updateCompass(270);
                rotation = 270;
                break;
            default:
                break;
        }
    }


    // Setters.
    public void setStartTime(float startTime) {
        this.startTime = startTime;
        currentTime = startTime;
        previousTime = 0;
        gameController.player.setStartTime(startTime);
    }


    public void setStartingPosition(){
        startPosition = Position;
    }
}
