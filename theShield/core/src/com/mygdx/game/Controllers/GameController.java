package com.mygdx.game.Controllers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.mygdx.game.AI.AI;
import com.mygdx.game.Characters.Creatures.Enemy_Bat;
import com.mygdx.game.Characters.Player;
import com.mygdx.game.Collisions.Collision;
import com.mygdx.game.DataStructures.Enums;
import com.mygdx.game.Levels.Level;
import com.mygdx.game.Levels.Level_Normal_1;
import com.mygdx.game.Levels.Level_Tutorial;
import com.mygdx.game.Screens.CreditsScreen;
import com.mygdx.game.Screens.HelpScreen;
import com.mygdx.game.Screens.InGameScreen;
import com.mygdx.game.Screens.LoadGameScreen;
import com.mygdx.game.Screens.MainMenuScreen;
import com.mygdx.game.Screens.NewGameScreen;
import com.mygdx.game.Screens.OptionsScreen;
import com.mygdx.game.Screens.SaveGameScreen;

import java.lang.reflect.Constructor;

/**
 * Created by Solist on 25/03/2017.
 */

public class GameController implements GameController_Interface {

    public MainMenuScreen mainMenuScreen;
    public NewGameScreen newGameScreen;
    public LoadGameScreen loadGameScreen;
    public SaveGameScreen saveGameScreen;
    public OptionsScreen optionsScreen;
    public HelpScreen helpScreen;
    public CreditsScreen creditsScreen;
    public InGameScreen inGameScreen;

    public float masterVolume = 1.0f;
    public float musicVolume = 1.0f;
    public float ambienceVolume = 1.0f;
    public float effectsVolume = 1.0f;
    public float dialogueVolume = 1.0f;

    public Boolean inGame = false;

    public Player player;

    public Level level;

    public Collision collisions;
    public AI ai;

    public CameraController inGameCamera;


    public void INIT() {
        readConfig();

        inGameCamera = new CameraController();
        inGameCamera.setGameController(this);

        assetManager.setGameController(this);
        assetManager.INIT();

        mainMenuScreen = new MainMenuScreen(this);
        newGameScreen = new NewGameScreen(this);
        loadGameScreen = new LoadGameScreen(this);
        saveGameScreen = new SaveGameScreen(this);
        optionsScreen = new OptionsScreen(this);
        helpScreen = new HelpScreen(this);
        creditsScreen = new CreditsScreen(this);
        inGameScreen = new InGameScreen(this);

        collisions = new Collision(this);
        ai = new AI(this);
    }

    public void newGame() {
        // Create the current level.
        level = new Level_Tutorial(this);
        // Set each game object to reference this gameController.
        player = new Player(this, newGameScreen.nameInput.getText(), Enums.Direction.EAST, level);
        // Set the game timer to 0;
        inGameScreen.gameTime = 0;
    }

    public void nextLevel(String levelName, Player player) {
        inGameScreen.hide();

        this.level.dispose();

        // Create the next level.
        Class<?> levelClass;
        try {
            levelClass = Class.forName(levelName);
            Constructor<?> levelConstructor = levelClass.getConstructor(GameController.class);
            Object levelObject = levelConstructor.newInstance(this);
            level = ((Level)levelObject);
        }
        catch (Exception e) {

        }

        // Create the player.
        this.player = new Player(player, Enums.Direction.EAST, level);

        inGameScreen.show();

        this.player.cellX = (int)(this.player.Position.x / 128 - 64);
        this.player.cellY = (int)(this.player.Position.y / 128 - 64);


        switch (player.Direction) {
            case NORTH:
                this.level.positionsFlaggedForMoving[player.cellX + 1][player.cellY] = true;
                break;
            case EAST:
                this.level.positionsFlaggedForMoving[player.cellX][player.cellY + 1] = true;
                break;
            case SOUTH:
                this.level.positionsFlaggedForMoving[player.cellX - 1][player.cellY] = true;
                break;
            case WEST:
                this.level.positionsFlaggedForMoving[player.cellX][player.cellY - 1] = true;
                break;
            default:
                break;
        }



        inGameCamera.setStartingPosition();
        this.player.setStartingPosition();
        float delta = Gdx.graphics.getDeltaTime();
        inGameCamera.setStartTime(delta);
        this.player.setStartTime(delta);
        this.player.isMoving = true;
        this.player.movementSoundIntervals = new boolean[this.player.playerSounds[4].length];
    }

    public void loadGame(String level, Player player) {
        // Create the current level.

        // Set each game object to reference this gameController.
        //player = new Player(this, Enums.Direction.NORTH, level);
    }

    public void dispose() {
        if (level != null) {
            level.dispose();
        }

        assetManager.dispose();
    }


    public void updateVolume() {
        updateMusicVolume();
        updateAmbienceVolume();
        updateEffectsVolume();
        updateDialogueVolume();
    }

    public void updateMusicVolume() {
        assetManager.menuMusic.setVolume(musicVolume * masterVolume);
        // Update game music
    }

    public void updateAmbienceVolume() {
        float resultVolume = ambienceVolume * masterVolume;
        // Update ambience music.

        // Update ambience noises.

    }

    public void updateEffectsVolume() {
        float resultVolume = effectsVolume * masterVolume;
        // Update player Spell Sounds


        // Update creature sounds.
        // Update creature weapon sounds.
        // Update creature Spell Sounds


        // Update trap sounds.
        // Update trap Spell Sounds


        // Update buff sounds.

    }

    public void updateDialogueVolume() {
        // Update player dialogue sounds.

        // Update boss dialogue sounds.

        // Update NPC dialogue sounds.

    }


    public void readConfig() {
        FileHandle file = Gdx.files.local("config.txt");

        if (file.exists() != true) {
            writeConfig();
            return;
        }

        String contents = file.readString();
        String wordsArray[] = contents.split("\\r?\\n");

        if (wordsArray[0].equals("TheShield.Config") != true) {
            return;
        }

        masterVolume = Float.parseFloat(wordsArray[3].substring(15));
        musicVolume = Float.parseFloat(wordsArray[4].substring(14));
        ambienceVolume = Float.parseFloat(wordsArray[5].substring(17));
        effectsVolume = Float.parseFloat(wordsArray[6].substring(16));
        dialogueVolume = Float.parseFloat(wordsArray[7].substring(17));
    }


    public void writeConfig() {
        String contents = "TheShield.Config\r\n" +
                          "\r\n" +
                          "Volume:\r\n" +
                          "MasterVolume = " + masterVolume + "\r\n" +
                          "MusicVolume = " + musicVolume + "\r\n" +
                          "AmbienceVolume = " + ambienceVolume + "\r\n" +
                          "EffectsVolume = " + effectsVolume + "\r\n" +
                          "DialogueVolume = " + dialogueVolume;
        FileHandle file = Gdx.files.local("config.txt");
        file.writeString(contents, false);
    }

    public void writeSave() {
        String contents = "TheShield.Save\r\n" +
                "\r\n" +
                "Save:\r\n" +
                "PlayerName = " + player.actorName + "\r\n" +
                "PlayerLevel = " + level + "\r\n" +
                "PlayingTime = " + player.currentTime + "\r\n" +
                "PlayerLives = " + player.lives + "\r\n" +
                "PlayerMaxLives = " + player.totalLives;
        FileHandle file = Gdx.files.local("save.txt");
        file.writeString(contents, false);
    }
    // Setters.

}
