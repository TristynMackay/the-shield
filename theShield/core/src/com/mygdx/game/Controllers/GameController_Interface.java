package com.mygdx.game.Controllers;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.mygdx.game.DataStructures.Enums;

/**
 * Created by Solist on 10/04/2017.
 */

public interface GameController_Interface {

    AssetController assetManager = new AssetController();

    Enums enums = new Enums();

    OrthographicCamera uiCamera = new OrthographicCamera();

}
