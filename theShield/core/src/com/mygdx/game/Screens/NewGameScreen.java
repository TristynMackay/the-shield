package com.mygdx.game.Screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.mygdx.game.Controllers.GameController;

/**
 * Created by Solist on 26/03/2017.
 */

public class NewGameScreen extends GameScreen implements Screen {

    public TextField nameInput;
    public CheckBox maleCheckBox;
    public CheckBox femaleCheckBox;

    public NewGameScreen(GameController gameController) {
        this.gameController = gameController;

        skin = gameController.assetManager.menuSkin;

        sprites = gameController.assetManager.menuSprites;
    }

    @Override
    public void show() {
        gameController.assetManager.setStage(gameController.assetManager.menuSprites);

        // Create a table.
        Table mainTable = new Table();
        // Make the table fill the stage.
        mainTable.setFillParent(true);
        // Set the alignment of the tables contents.
        mainTable.top();

        // Create the screen's title.
        Label newGameTitle = new Label("New Game", skin, "label_Large");

        // Create the screen's content titles.
        Label characterOptionsLabel = new Label("Character Options", skin);
        Label characterLabel = new Label("Character", skin);

        // Create the screen's inputs.
        Label characterName = new Label("Character Name", skin, "label_Small");
        nameInput = new TextField("", skin, "textField");
        Label genderLabel = new Label("Gender", skin, "label_Small");
        maleCheckBox = new CheckBox("Male", skin);
        femaleCheckBox = new CheckBox("Female", skin);
        Label hairLabel = new Label("Hair Style", skin, "label_Small");
        TextButton hairLeftButton = new TextButton("<-", skin, "textButton");
        TextButton hairRightButton = new TextButton("->", skin, "textButton");
        Label hairColourLabel = new Label("Hair Colour", skin, "label_Small");
        TextButton hairColourLeftButton = new TextButton("<-", skin, "textButton");
        TextButton hairColourRightButton = new TextButton("->", skin, "textButton");
        Label eyeColourLabel = new Label("Eye Colour", skin, "label_Small");
        TextButton eyeColourLeftButton = new TextButton("<-", skin, "textButton");
        TextButton eyeColourRightButton = new TextButton("->", skin, "textButton");
        Label featuresLabel = new Label("Features", skin, "label_Small");
        TextButton featuresLeftButton = new TextButton("<-", skin, "textButton");
        TextButton featuresRightButton = new TextButton("->", skin, "textButton");


        // Create the screen's buttons.
        TextButton startGameButton = new TextButton("Start", skin, "textButton");
        TextButton backButton = new TextButton("Back", skin, "textButton");


        startGameButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                gameController.newGame();
                ((Game) Gdx.app.getApplicationListener()).setScreen(gameController.inGameScreen);
            }
        });

        backButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                ((Game) Gdx.app.getApplicationListener()).setScreen(gameController.mainMenuScreen);
            }
        });

        Table optionsTable = new Table();

        // Add the options to the options table.
        optionsTable.add().width(10);
        optionsTable.add(characterName).colspan(3);
        optionsTable.add().width(10);
        optionsTable.row();
        optionsTable.add().width(10);
        optionsTable.add(nameInput).colspan(3);
        optionsTable.add().width(10);
        optionsTable.row();
        optionsTable.add().height(20).colspan(5);
        optionsTable.row();
        optionsTable.add().width(10);
        optionsTable.add(genderLabel).colspan(3);
        optionsTable.add().width(10);
        optionsTable.row();
        optionsTable.add().width(10);
        optionsTable.add(maleCheckBox);
        optionsTable.add();
        optionsTable.add(femaleCheckBox);
        optionsTable.add().width(10);
        optionsTable.row();
        optionsTable.add().height(20).colspan(5);
        optionsTable.row();
        optionsTable.add().width(10);
        optionsTable.add(hairLabel).colspan(3);
        optionsTable.add().width(10);
        optionsTable.row();
        optionsTable.add().width(10);
        optionsTable.add(hairLeftButton).width(40).right();
        optionsTable.add().width(10);
        optionsTable.add(hairRightButton).width(40).left();
        optionsTable.add().width(10);
        optionsTable.row();
        optionsTable.add().height(20).colspan(5);
        optionsTable.row();
        optionsTable.add().width(10);
        optionsTable.add(hairColourLabel).colspan(3);
        optionsTable.add().width(10);
        optionsTable.row();
        optionsTable.add().width(10);
        optionsTable.add(hairColourLeftButton).width(40).right();
        optionsTable.add().width(10);
        optionsTable.add(hairColourRightButton).width(40).left();
        optionsTable.add().width(10);
        optionsTable.row();
        optionsTable.add().height(20).colspan(5);
        optionsTable.row();
        optionsTable.add().width(10);
        optionsTable.add(eyeColourLabel).colspan(3);
        optionsTable.add().width(10);
        optionsTable.row();
        optionsTable.add().width(10);
        optionsTable.add(eyeColourLeftButton).width(40).right();
        optionsTable.add().width(10);
        optionsTable.add(eyeColourRightButton).width(40).left();
        optionsTable.add().width(10);
        optionsTable.row();
        optionsTable.add().height(20).colspan(5);
        optionsTable.row();
        optionsTable.add().width(10);
        optionsTable.add(featuresLabel).colspan(3);
        optionsTable.add().width(10);
        optionsTable.row();
        optionsTable.add().width(10);
        optionsTable.add(featuresLeftButton).width(40).right();
        optionsTable.add().width(10);
        optionsTable.add(featuresRightButton).width(40).left();
        optionsTable.add().width(10);

        ScrollPane scrollPane = new ScrollPane(optionsTable, skin);

        // Add the buttons to the tables.
        mainTable.add(newGameTitle).expandX().top().colspan(4);
        mainTable.row();
        mainTable.add().height(10).expandX().colspan(4);
        mainTable.row();
        mainTable.add().width(20);
        mainTable.add(characterOptionsLabel);
        mainTable.add(characterLabel);
        mainTable.add().width(20);
        mainTable.row();
        mainTable.add().width(20);
        mainTable.add(scrollPane).expandY();
        mainTable.add().expand();
        mainTable.add().width(20);
        mainTable.row();
        mainTable.add().height(10).expandX().colspan(4);
        mainTable.row();
        mainTable.add().width(20);
        mainTable.add(backButton).left().width(100);
        mainTable.add(startGameButton).right().width(100);
        mainTable.add().width(20);

        // Add the table to the stage.
        gameController.assetManager.stage.addActor(mainTable);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0.0f, 0.0f, 0.0f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        // Music loop
        if (gameController.assetManager.menuMusic.getPosition() >= 32.3f) {
            gameController.assetManager.menuMusic.setPosition(8.2f);
        }

        gameController.assetManager.stage.act();
        gameController.assetManager.stage.draw();
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {

    }


    @Override
    public void resize(int width, int height) {
        gameController.assetManager.viewport.update(width, height);
        gameController.uiCamera.position.set(gameController.uiCamera.viewportWidth / 2,
                                            gameController.uiCamera.viewportHeight / 2, 0);
        gameController.uiCamera.update();
    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}

