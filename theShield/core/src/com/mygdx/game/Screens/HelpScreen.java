package com.mygdx.game.Screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.mygdx.game.Controllers.GameController;

/**
 * Created by Solist on 26/03/2017.
 */

public class HelpScreen extends GameScreen implements Screen {

    public HelpScreen(GameController gameController) {
        this.gameController = gameController;

        skin = gameController.assetManager.menuSkin;

        sprites = gameController.assetManager.menuSprites;
    }

    @Override
    public void show() {
        gameController.assetManager.setStage(gameController.assetManager.menuSprites);

        // Create a table.
        Table mainTable = new Table();
        // Make the table fill the stage.
        mainTable.setFillParent(true);
        // Set the alignment of the tables contents.
        mainTable.top();

        // Create the screen's title.
        Label helpTitle = new Label("Help", skin, "label_Large");

        // Create the screen's buttons.
        TextButton backButton = new TextButton("Back", skin, "textButton");


        backButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (gameController.inGame) {
                    ((Game) Gdx.app.getApplicationListener()).setScreen(gameController.inGameScreen);
                } else {
                    ((Game) Gdx.app.getApplicationListener()).setScreen(gameController.mainMenuScreen);
                }
            }
        });

        // Create and add contents to the help table.
        Label movementLabel = new Label("Movement", skin, "label_Large_Gold");
        Label moveForwardLabel = new Label("Move Forward", skin);
        Label rotateLeftLabel = new Label("Rotate Left", skin);
        Label rotateRightLabel = new Label("Rotate Right", skin);
        Label livesLabel = new Label("Lives", skin, "label_Large_Gold");
        Label combatLabel = new Label("Combat", skin, "label_Large_Gold");
        Label meleeLabel = new Label("Melee Attack", skin);
        Label blockLabel = new Label("Blocking", skin);
        Label rangedLabel = new Label("Ranged Attack", skin);
        Label switchLabel = new Label("Weapon Switching", skin, "label_Large_Gold");
        Label meleeSwitchLabel = new Label("Melee Switch", skin);
        Label rangedSwitchLabel = new Label("Ranged Switch", skin);
        Label buffsLabel = new Label("Blessings", skin, "label_Large_Gold");
        Label compassLabel = new Label("Compass", skin, "label_Large_Gold");
        Label creaturesSlainLabel = new Label("Creatures Slain", skin, "label_Large_Gold");
        Label timerLabel = new Label("Timer", skin, "label_Large_Gold");
        Label menuLabel = new Label("Menu", skin, "label_Large_Gold");

        String moveForwardString = "";
        String rotateLeftString = "";
        String rotateRightString = "";
        String livesString = "";
        String meleeString = "";
        String blockString = "";
        String rangedString = "";
        String meleeSwitchString = "";
        String rangedSwitchString = "";
        String buffsString = "";
        String compassString = "";
        String creaturesSlainString = "";
        String timerString = "";
        String menuString = "";

        livesString = "You can see your lives as hearts located next to the menu button " +
                    "in the top left corner of your screen.";
        compassString = "You can track which direction you are facing by looking at the " +
                        "compass located in the top right corner of your screen.";
        creaturesSlainString = "You can track how many creatures you have slain by looking at the " +
                                "information text in the top right corner of your screen.";
        buffsString = "You can see how many blessings you currently have by looking at " +
                        "the left hand side of your screen. Blessings give bonuses to your play." +
                        " Buffs with the golden + on them indicate that they are permanent.";
        timerString = "You can identify the time it has taken for you to progress through the " +
                        "dungeon by looking at the timer next to the compass in the top right " +
                        "corner of the screen.";

        switch (Gdx.app.getType()) {
            case Android:
                moveForwardString = "You can move forward by tilting your phone forward.";
                rotateLeftString = "You can rotate left by tilting your phone to the left.";
                rotateRightString = "You can rotate right by tilting your phone to the right";
                meleeString = "You can attack with your melee weapon by pressing the sword button " +
                                "located in the bottom right corner of your screen.";
                blockString = "You can block attacks with your shield by pressing the shield button " +
                                "located next to the sword button in the bottom right corner of your screen.";
                rangedString = "You can attack with your ranged weapon by pressing the bow button " +
                                "located in the bottom right corner of your screen.";
                meleeSwitchString = "You can switch to your melee weapon and shield by pressing the " +
                                    "switch button located in the bottom left corner of your screen.";
                rangedSwitchString = "You can switch to your ranged weapon by pressing the switch " +
                                    "button located in the bottom left corner of your screen.";
                menuString = "You can open the in game menu at any time by pressing the menu button " +
                                "located in the top left corner of your screen.";
                break;
            case Desktop:
                moveForwardString = "You can move forward by pressing the W key on your keyboard.";
                rotateLeftString = "You can rotate left by pressing the A key on your keyboard.";
                rotateRightString = "You can rotate right by pressing the D key on your keyboard.";
                meleeString = "You can attack with your melee weapon by pressing the left click " +
                                "button on your mouse.";
                blockString = "You can block attacks with your shield by pressing the right click " +
                            "button on your mouse.";
                rangedString = "You can attack with your ranged weapon by pressing the left click " +
                                "button on your mouse.";
                meleeSwitchString = "You can switch to your melee weapon and shield by pressing the " +
                                    "Tab key on your keyboard.";
                rangedSwitchString = "You can switch to your ranged weapon by pressing the Tab key " +
                                        "on your keyboard.";
                menuString = "You can open the in game menu at any time by pressing the menu button " +
                                "located in the top left corner of your screen or by pressing the " +
                                "Escape key on your keyboard.";
                break;
            case iOS:
                moveForwardString = "You can move forward by tilting your phone forward.";
                rotateLeftString = "You can rotate left by tilting your phone to the left.";
                rotateRightString = "You can rotate right by tilting your phone to the right";
                meleeString = "You can attack with your melee weapon by pressing the sword button " +
                                "located in the bottom right corner of your screen.";
                blockString = "You can block attacks with your shield by pressing the shield button " +
                                "located next to the sword button in the bottom right corner of your screen.";
                rangedString = "You can attack with your ranged weapon by pressing the bow button " +
                                "located in the bottom right corner of your screen.";
                meleeSwitchString = "You can switch to your melee weapon and shield by pressing the " +
                                    "switch button located in the bottom left corner of your screen.";
                rangedSwitchString = "You can switch to your ranged weapon by pressing the switch " +
                                        "button located in the bottom left corner of your screen.";
                menuString = "You can open the in game menu at any time by pressing the menu button " +
                                "located in the top left corner of your screen.";
                break;
            default:
                break;
        }

        Label moveForwardText = new Label(moveForwardString, skin, "label_Small");
        Label rotateLeftText = new Label(rotateLeftString, skin, "label_Small");
        Label rotateRightText = new Label(rotateRightString, skin, "label_Small");
        Label livesText = new Label(livesString, skin, "label_Small");
        Label meleeText = new Label(meleeString, skin, "label_Small");
        Label blockText = new Label(blockString, skin, "label_Small");
        Label rangedText = new Label(rangedString, skin, "label_Small");
        Label meleeSwitchText = new Label(meleeSwitchString, skin, "label_Small");
        Label rangedSwitchText = new Label(rangedSwitchString, skin, "label_Small");
        Label buffsText = new Label(buffsString, skin, "label_Small");
        Label compassText = new Label(compassString, skin, "label_Small");
        Label creaturesSlainText = new Label(creaturesSlainString, skin, "label_Small");
        Label timerText = new Label(timerString, skin, "label_Small");
        Label menuText = new Label(menuString, skin, "label_Small");

        moveForwardText.setWrap(true);
        rotateLeftText.setWrap(true);
        rotateRightText.setWrap(true);
        livesText.setWrap(true);
        meleeText.setWrap(true);
        blockText.setWrap(true);
        rangedText.setWrap(true);
        meleeSwitchText.setWrap(true);
        rangedSwitchText.setWrap(true);
        buffsText.setWrap(true);
        compassText.setWrap(true);
        creaturesSlainText.setWrap(true);
        timerText.setWrap(true);
        menuText.setWrap(true);

        Button moveForwardImage = new Button(gameController.assetManager.toolTipMovementSkin, "moveForward");
        Button rotateLeftImage = new Button(gameController.assetManager.toolTipMovementSkin, "rotateLeft");
        Button rotateRightImage = new Button(gameController.assetManager.toolTipMovementSkin, "rotateRight");
        Button livesImage = new Button(gameController.assetManager.toolTipGeneralSkin, "lifeImage");
        Button meleeImage = new Button(gameController.assetManager.toolTipCombatSkin, "meleeButtonImage");
        Button blockImage = new Button(gameController.assetManager.toolTipCombatSkin, "blockButtonImage");
        Button rangedImage = new Button(gameController.assetManager.toolTipCombatSkin, "rangedButtonImage");
        Button meleeSwitchImage = new Button(gameController.assetManager.toolTipCombatSkin, "switchMeleeImage");
        Button rangedSwitchImage = new Button(gameController.assetManager.toolTipCombatSkin, "switchRangedImage");
        Button buffsImage = new Button(gameController.assetManager.toolTipGeneralSkin, "buffsImage");
        Button compassImage = new Button(gameController.assetManager.toolTipGeneralSkin, "compassImage");
        Button creaturesSlainImage = new Button(gameController.assetManager.toolTipGeneralSkin, "creaturesSlainImage");
        Button timerImage = new Button(gameController.assetManager.toolTipGeneralSkin, "timerImage");
        Button menuButtonImage = new Button(gameController.assetManager.toolTipGeneralSkin, "menuButtonImage");

        Table contentsTable = new Table();

        // Add the help contents to the contents table.
        // Movement help section.
        contentsTable.add(movementLabel).colspan(2);
        contentsTable.row();
        contentsTable.add(moveForwardLabel);
        contentsTable.add();
        contentsTable.row();
        contentsTable.add(moveForwardText).width(160).height(140).top();
        contentsTable.add(moveForwardImage).width(200).height(120);
        contentsTable.row();
        contentsTable.add().height(10).expandX().colspan(2);
        contentsTable.row();
        contentsTable.add();
        contentsTable.add(rotateLeftLabel);
        contentsTable.row();
        contentsTable.add(rotateLeftImage).width(200).height(120);
        contentsTable.add(rotateLeftText).width(160).height(140).top();
        contentsTable.row();
        contentsTable.add().height(10).expandX().colspan(2);
        contentsTable.row();
        contentsTable.add(rotateRightLabel);
        contentsTable.add();
        contentsTable.row();
        contentsTable.add(rotateRightText).width(160).height(140).top();
        contentsTable.add(rotateRightImage).width(200).height(120);
        contentsTable.row();
        contentsTable.add().height(40).expandX().colspan(2);
        contentsTable.row();
        // Life help section.
        contentsTable.add(livesLabel).colspan(2);
        contentsTable.row();
        contentsTable.add(livesText).width(160).height(140).top();
        contentsTable.add(livesImage).width(200).height(120);
        contentsTable.row();
        contentsTable.add().height(40).expandX().colspan(2);
        contentsTable.row();
        // Combat help section.
        contentsTable.add(combatLabel).colspan(2);
        contentsTable.row();
        contentsTable.add(meleeLabel);
        contentsTable.add();
        contentsTable.row();
        contentsTable.add(meleeText).width(160).height(140).top();
        contentsTable.add(meleeImage).width(200).height(120);
        contentsTable.row();
        contentsTable.add().height(10).expandX().colspan(2);
        contentsTable.row();
        contentsTable.add();
        contentsTable.add(blockLabel);
        contentsTable.row();
        contentsTable.add(blockImage).width(200).height(120);
        contentsTable.add(blockText).width(160).height(140).top();
        contentsTable.row();
        contentsTable.add().height(10).expandX().colspan(2);
        contentsTable.row();
        contentsTable.add(rangedLabel);
        contentsTable.add();
        contentsTable.row();
        contentsTable.add(rangedText).width(160).height(140).top();
        contentsTable.add(rangedImage).width(200).height(120);
        contentsTable.row();
        contentsTable.add().height(40).expandX().colspan(2);
        contentsTable.row();
        // Weapon switch help section.
        contentsTable.add(switchLabel).colspan(2);
        contentsTable.row();
        contentsTable.add(meleeSwitchLabel);
        contentsTable.add();
        contentsTable.row();
        contentsTable.add(meleeSwitchText).width(160).height(140).top();
        contentsTable.add(meleeSwitchImage).width(200).height(120);
        contentsTable.row();
        contentsTable.add().height(10).expandX().colspan(2);
        contentsTable.row();
        contentsTable.add();
        contentsTable.add(rangedSwitchLabel);
        contentsTable.row();
        contentsTable.add(rangedSwitchImage).width(200).height(120);
        contentsTable.add(rangedSwitchText).width(160).height(140).top();
        contentsTable.row();
        contentsTable.add().height(40).expandX().colspan(2);
        contentsTable.row();
        // Blessings help section.
        contentsTable.add(buffsLabel).colspan(2);
        contentsTable.row();
        contentsTable.add(buffsText).width(160).height(140).top();
        contentsTable.add(buffsImage).width(200).height(120);
        contentsTable.row();
        contentsTable.add().height(40).expandX().colspan(2);
        contentsTable.row();
        // Compass help section.
        contentsTable.add(compassLabel).colspan(2);
        contentsTable.row();
        contentsTable.add(compassText).width(160).height(140).top();
        contentsTable.add(compassImage).width(200).height(120);
        contentsTable.row();
        contentsTable.add().height(40).expandX().colspan(2);
        contentsTable.row();
        // Creatures Slain help section.
        contentsTable.add(creaturesSlainLabel).colspan(2);
        contentsTable.row();
        contentsTable.add(creaturesSlainText).width(160).height(140).top();
        contentsTable.add(creaturesSlainImage).width(200).height(120);
        contentsTable.row();
        contentsTable.add().height(40).expandX().colspan(2);
        contentsTable.row();
        // Timer help section.
        contentsTable.add(timerLabel).colspan(2);
        contentsTable.row();
        contentsTable.add(timerText).width(160).height(140).top();
        contentsTable.add(timerImage).width(200).height(120);
        contentsTable.row();
        contentsTable.add().height(40).expandX().colspan(2);
        contentsTable.row();
        // Menu help section.
        contentsTable.add(menuLabel).colspan(2);
        contentsTable.row();
        contentsTable.add(menuText).width(160).height(140).top();
        contentsTable.add(menuButtonImage).width(200).height(120);


        ScrollPane scrollPane = new ScrollPane(contentsTable, skin);

        // Add the buttons to the tables.
        mainTable.add(helpTitle).expand().top().colspan(5);
        mainTable.row();
        mainTable.add(scrollPane).expand().colspan(5);
        mainTable.row();
        mainTable.add().height(10).expandX().colspan(5);
        mainTable.row();
        mainTable.add().width(20);
        mainTable.add(backButton).width(100).left();
        mainTable.add().expandX();
        mainTable.add().width(100).right();
        mainTable.add().width(20);

        // Add the table to the stage.
        gameController.assetManager.stage.addActor(mainTable);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0.0f, 0.0f, 0.0f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        // Music loop
        if (gameController.assetManager.menuMusic.getPosition() >= 32.3f) {
            gameController.assetManager.menuMusic.setPosition(8.2f);
        }

        //displayBackground(gameController.assetManager.backgroundSprite,
        //                    gameController.assetManager.backgroundTexture);

        gameController.assetManager.stage.act();
        gameController.assetManager.stage.draw();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }


    @Override
    public void resize(int width, int height) {
        gameController.assetManager.viewport.update(width, height);
        gameController.uiCamera.position.set(gameController.uiCamera.viewportWidth / 2,
                                            gameController.uiCamera.viewportHeight / 2, 0);
        gameController.uiCamera.update();
    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
