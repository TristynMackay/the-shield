package com.mygdx.game.Screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.mygdx.game.Blessings.Blessing;
import com.mygdx.game.Characters.Creature;
import com.mygdx.game.Controllers.GameController;
import com.mygdx.game.DataStructures.Enums;
import com.mygdx.game.Traps.Trap;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Solist on 26/03/2017.
 */

public class InGameScreen extends GameScreen implements Screen {

    // Variables.
    public boolean gamePaused = true;

    public boolean uiRendered = false;

    public double gameTime = 0;

    private Button menuButton;
    public Button swapSwordButton;
    public Button swapBowButton;
    public Button shieldButton;
    public Button swordButton;
    public Button bowButton;

    private boolean playerStopBlock = true;

    private boolean weaponChanged = false;

    private Label playerNameText;

    private Label timerText;
    private Label creaturesSlainText;

    private Button[] lifeImages;
    private TextButton[] buffImages;

    private Button compassImage;

    private Table compassTable;

    private Table livesTable;

    public Table buffsTable;

    private Table mainTable;

    private Table menuTable;

    private Table deathMenuTable;
    public Label deathText;
    public String deathString = "You Have Been Slain By\nA ";

    private Skin menuSkin;

    private SpriteBatch gameBatch;

    public Music ambienceMusic;

    public boolean menuActive = false;
    private boolean menuDisplayed = false;


    public InGameScreen(GameController gameController) {
        this.gameController = gameController;

        menuSkin = gameController.assetManager.menuSkin;

        skin = gameController.assetManager.uiSkin;

        sprites = gameController.assetManager.uiSprites;

        gameBatch = new SpriteBatch();
    }


    public void show() {
        float w = Gdx.graphics.getWidth();
        float h = Gdx.graphics.getHeight();

        gameController.inGame = true;
        if (menuActive == false) {
            gamePaused = false;
        }

        gameController.inGameCamera.camera = new OrthographicCamera();
        gameController.inGameCamera.camera.setToOrtho(false, 896, 640);
        gameController.inGameCamera.camera.update();
        gameController.inGameCamera.Direction = gameController.player.Direction;
        gameController.inGameCamera.startCamera();
        gameController.level.updateLevel(gameController.inGameCamera.Direction);

        gameController.inGameCamera.Position = gameController.player.Position;

        gameController.assetManager.setStage(sprites);

        // Create the menu table.
        menuTable = new Table();
        // Make the table fill the stage.
        menuTable.setFillParent(true);
        // Set the alignment of the table's contents.
        menuTable.top();
        // Create the body table for the menu.
        Table menuBodyTable = new Table();

        // Create the table's title.
        Label menuTitle = new Label("Menu", menuSkin, "label_Large");

        // Create the table's buttons.
        TextButton resumeButton = new TextButton("Resume", menuSkin, "textButton");
        TextButton saveButton = new TextButton("Save", menuSkin, "textButton");
        TextButton loadButton = new TextButton("Load", menuSkin, "textButton");
        TextButton optionsButton = new TextButton("Options", menuSkin, "textButton");
        TextButton helpButton = new TextButton("Help", menuSkin, "textButton");
        TextButton exitButton = new TextButton("Exit", menuSkin, "textButton");


        resumeButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                gamePaused = false;
                menuActive = false;
                uiRendered = false;
            }
        });

        saveButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                // Show Save Screen.
                uiRendered = false;
                menuActive = true;
                ((Game)Gdx.app.getApplicationListener()).setScreen(gameController.saveGameScreen);
            }
        });

        loadButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                // Show Load Screen.
                uiRendered = false;
                menuActive = true;
                ((Game)Gdx.app.getApplicationListener()).setScreen(gameController.loadGameScreen);
            }
        });

        optionsButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                // Show Help Screen.
                uiRendered = false;
                menuActive = true;
                ((Game)Gdx.app.getApplicationListener()).setScreen(gameController.optionsScreen);
            }
        });

        helpButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                // Show Help Screen.
                uiRendered = false;
                menuActive = true;
                ((Game)Gdx.app.getApplicationListener()).setScreen(gameController.helpScreen);
            }
        });

        exitButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                // Make the changes.
                gameController.inGame = false;
                uiRendered = false;
                menuActive = false;
                // Return to the previous screen.
                ((Game) Gdx.app.getApplicationListener()).setScreen(gameController.mainMenuScreen);
            }
        });

        // Add the buttons to the body table.
        menuBodyTable.add(resumeButton).width(100);
        menuBodyTable.row();
        menuBodyTable.add(saveButton).width(100);
        menuBodyTable.row();
        menuBodyTable.add(loadButton).width(100);
        menuBodyTable.row();
        menuBodyTable.add(optionsButton).width(100);
        menuBodyTable.row();
        menuBodyTable.add(helpButton).width(100);
        menuBodyTable.row();
        menuBodyTable.add(exitButton).width(100);

        // Add the contents to the main table.
        menuTable.add(menuTitle);
        menuTable.row();
        menuTable.add(menuBodyTable).expand();


        // Create the death menu table.
        deathMenuTable = new Table();
        // Make the table fill the stage.
        deathMenuTable.setFillParent(true);
        // Set the alignment of the table's contents.
        deathMenuTable.top();
        // Create the body table for the death menu.
        Table deathMenuBodyTable = new Table();

        // Create the table's title.
        Label deathMenuTitle = new Label("Menu", menuSkin, "label_Large");

        // Create the death text.
        deathText = new Label("You Have Been Slain!", menuSkin, "label_Death");
        deathText.setAlignment(Align.center);

        // Create the table's buttons.
        TextButton deathMenuLoadButton = new TextButton("Load", menuSkin, "textButton");
        TextButton deathMenuOptionsButton = new TextButton("Options", menuSkin, "textButton");
        TextButton deathMenuHelpButton = new TextButton("Help", menuSkin, "textButton");
        TextButton deathMenuExitButton = new TextButton("Exit", menuSkin, "textButton");


        deathMenuLoadButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                // Show Load Screen.
                uiRendered = false;
                menuActive = true;
                ((Game)Gdx.app.getApplicationListener()).setScreen(gameController.loadGameScreen);
            }
        });

        deathMenuOptionsButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                // Show Help Screen.
                uiRendered = false;
                menuActive = true;
                ((Game)Gdx.app.getApplicationListener()).setScreen(gameController.optionsScreen);
            }
        });

        deathMenuHelpButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                // Show Help Screen.
                uiRendered = false;
                menuActive = true;
                ((Game)Gdx.app.getApplicationListener()).setScreen(gameController.helpScreen);
            }
        });

        deathMenuExitButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                // Make the changes.
                gameController.inGame = false;
                uiRendered = false;
                menuActive = false;
                // Return to the previous screen.
                ((Game) Gdx.app.getApplicationListener()).setScreen(gameController.mainMenuScreen);
            }
        });

        // Add the buttons to the body table.
        deathMenuBodyTable.add(deathText).width(160).align(Align.center);
        deathMenuBodyTable.row();
        deathMenuBodyTable.add().height(20);
        deathMenuBodyTable.row();
        deathMenuBodyTable.add(deathMenuLoadButton).width(100);
        deathMenuBodyTable.row();
        deathMenuBodyTable.add(deathMenuOptionsButton).width(100);
        deathMenuBodyTable.row();
        deathMenuBodyTable.add(deathMenuHelpButton).width(100);
        deathMenuBodyTable.row();
        deathMenuBodyTable.add(deathMenuExitButton).width(100);

        // Add the contents to the main table.
        deathMenuTable.add(deathMenuTitle);
        deathMenuTable.row();
        deathMenuTable.add(deathMenuBodyTable).expand();


        // Create the ui table.
        mainTable = new Table();
        // Make the table fill the stage.
        mainTable.setFillParent(true);
        // Set the alignment of the table's contents.
        mainTable.top();

        // Create the screen's buttons.
        menuButton = new Button(skin, "MenuButton");
        swapSwordButton = new Button(skin, "SwapSwordButton");
        swapBowButton = new Button(skin, "SwapBowButton");
        shieldButton = new Button(skin, "ShieldButton");
        swordButton = new Button(skin, "SwordButton");
        bowButton = new Button(skin, "BowButton");
        // Create the screen's images.
        createLives();
        createBuffs();
        compassImage = new Button(skin, "Compass");
        // Create the screen's text.
        if (playerNameText == null) {
            playerNameText = new Label(gameController.player.playerName, skin, "label_Small");
        }
        if (timerText == null) {
            timerText = new Label("Time - 00:00:00", skin, "label_Small");
        }
        if (creaturesSlainText == null) {
            creaturesSlainText = new Label("Creatures Slain - ", skin, "label_Small");
        }

        menuButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                gamePaused = true;
                menuActive = true;
                menuDisplayed = false;
                uiRendered = false;
            }
        });

        swapSwordButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (gameController.player.isDead == false) {
                    if (gameController.player.performingActions() == false) {
                        // Switch weapons.
                        gameController.player.SwitchSwordAndShield();
                        weaponChanged = true;
                        uiRendered = false;
                    }
                }
            }
        });

        swapBowButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (gameController.player.isDead == false) {
                    if (gameController.player.performingActions() == false) {
                        // Switch weapons.
                        gameController.player.SwitchBowAndArrow();
                        weaponChanged = true;
                        uiRendered = false;
                    }
                }
            }
        });

        swordButton.addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }
        });

        shieldButton.addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                gameController.player.blockCanceled = true;
            }
        });

        bowButton.addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }
        });

        compassTable = new Table();
        compassTable.setTransform(true);
        compassTable.setOrigin(32, 32);
        compassImage.setOrigin(32, 32);
        compassTable.add(compassImage);
        updateCompass(gameController.inGameCamera.rotation);

        populateUITable();

        // Add the table to the stage.
        if (menuActive == true) {
            populateMenuTable();
        }
        else{
            gameController.assetManager.stage.addActor(mainTable);
        }


        // Start ambience music.
        gameController.inGameScreen.ambienceMusic.setLooping(true);
        gameController.inGameScreen.ambienceMusic.setVolume(gameController.ambienceVolume * gameController.masterVolume);
        gameController.inGameScreen.ambienceMusic.play();
    }


    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0.0f, 0.0f, 0.0f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        if (gameController.assetManager.menuMusic.isPlaying()) {
            gameController.assetManager.menuMusic.setVolume(gameController.assetManager.menuMusic.getVolume() / 1.4f);
            if (gameController.assetManager.menuMusic.getVolume() < 0.01f) {
                gameController.assetManager.menuMusic.stop();
            }
        }

        ambienceMusic.setVolume(gameController.ambienceVolume * gameController.masterVolume);


        gameController.assetManager.inputController.CheckInputs();

        if (gameController.player.isRotatingLeft) {
            if (gameController.player.isDead == false) {
                gameController.inGameCamera.TurnLeft(delta);
            }
        }
        else if (gameController.player.isRotatingRight) {
            if (gameController.player.isDead == false) {
                gameController.inGameCamera.TurnRight(delta);
            }
        }
        else if (gameController.player.isMoving) {
            gameController.inGameCamera.MoveForward(delta);
        }
        else if (gameController.player.isAttacking) {
            if (gameController.player.isDead == false) {
                if (gameController.player.attackMode == Enums.AttackType.MELEE) {
                    gameController.player.MeleeAttack(delta);
                } else {
                    gameController.player.RangedAttack(delta);
                }
            }
        }
        else if (gameController.player.isBlocking) {
            if (gameController.player.isDead == false) {
                gameController.player.Block(delta);
            }
        }
        else if (gameController.player.isSwitchingWeapons) {
            if (gameController.player.isDead == false) {
                gameController.player.switchWeaponCurrentTime += delta;
                if (gameController.player.switchWeaponCurrentTime > gameController.player.switchWeaponStartTime + gameController.player.switchWeaponSpeed) {
                    gameController.player.isSwitchingWeapons = false;
                }
            }
        }

        if (gameController.player.isDead == false && gameController.player.performingActions() == false) {
            if (swordButton.isPressed()) {
                // Melee Attack.
                gameController.player.MeleeAttack(delta);
            }
            else if (shieldButton.isPressed()) {
                // Block.
                gameController.player.Block(delta);
            }
            else if (bowButton.isPressed()) {
                // Ranged Attack.
                gameController.player.RangedAttack(delta);
            }
        }


        gameController.inGameCamera.camera.update();
        gameController.level.mapRenderer.setView(gameController.inGameCamera.camera);
        gameController.level.mapRenderer.render();


        if (gamePaused == false) {
            gameBatch.setProjectionMatrix(gameController.inGameCamera.camera.combined);

            // Render the traps and buffs.
            gameBatch.begin();
            int buffIndex = 0;
            for (int i = 0; i < gameController.level.buffList.length; i++) {
                Blessing blessing = gameController.level.buffList[i];
                if (blessing != null && blessing.render(delta, gameBatch) == false) {
                    gameController.level.buffList[i] = null;
                }
            }
            for (int i = 0; i < gameController.level.trapList.length; i++) {
                Trap trap = gameController.level.trapList[i];
                if (trap != null && trap.render(delta, gameBatch) == false) {
                    gameController.level.trapList[i] = null;
                }
            }
            gameBatch.end();

            // Render the player/enemies.
            gameBatch.begin();
            // Render the enemies.
            for (int i = 0; i < gameController.level.creatureList.length; i++) {
                Creature enemy = gameController.level.creatureList[i];
                enemy.render(delta, gameBatch, new Vector2(enemy.playerObject.getRectangle().x, enemy.playerObject.getRectangle().y));
            }
            // Render the player.
            gameController.player.render(delta, gameBatch, new Vector2(gameController.player.playerObject.getRectangle().x, gameController.player.playerObject.getRectangle().y));
            gameBatch.end();

            // Render the projectiles.
            gameBatch.begin();
            for (int i = 0; i < gameController.level.projectileList.length; i++) {
                if (gameController.level.projectileList[i] != null) {
                    gameController.level.projectileList[i].render(delta, gameBatch);
                }
            }
            gameBatch.end();

            // Render the special effects.
            gameBatch.begin();

            gameBatch.end();

            // Render the fog.
            gameBatch.begin();

            gameBatch.end();

            // Render the UI.
            if (gameController.player.livesChanged == true) {
                gameController.player.livesChanged = false;
                updateLives();
            }


            if (gameController.player.isDead == false) {
                gameTime += delta;
                updateTimer((float) gameTime);
                updateCreaturesSlain();
                updateBuffs();
            }
            else {
                // Display death menu.
            }
        }

        if (uiRendered == false) {
            if (weaponChanged == true) {
                populateUITable();
            }

            // Render the menu.
            if (menuActive == true) {
                if (menuDisplayed == false) {
                    menuDisplayed = true;
                    populateMenuTable();
                }
            } else {
                if (menuDisplayed == true) {
                    menuDisplayed = false;
                    populateUITable();
                }
            }
            uiRendered = true;
        }

        gameController.assetManager.stage.act();
        gameController.assetManager.stage.draw();

        if (gameController.player.isDead == false &&
                gameController.player.attackMode == Enums.AttackType.MELEE) {
            InputEvent buttonEvent = new InputEvent();
            if (Gdx.input.isKeyJustPressed(Input.Keys.E)) {
                if (gameController.inGameScreen.shieldButton.getClickListener().isPressed() == false) {
                    buttonEvent.setType(InputEvent.Type.touchDown);
                    gameController.inGameScreen.shieldButton.fire(buttonEvent);
                }
                else {
                    buttonEvent.setType(InputEvent.Type.touchUp);
                    gameController.inGameScreen.shieldButton.fire(buttonEvent);
                }
            }
        }
    }


    @Override
    public void pause() {
    }

    @Override
    public void resume() {

    }


    @Override
    public void resize(int width, int height) {
        gameController.assetManager.viewport.update(width, height);

        gameController.inGameCamera.camera.position.set(gameController.player.Position.x,
                gameController.player.Position.y, 0);
        gameController.inGameCamera.camera.update();

        gameBatch.getProjectionMatrix().setToOrtho2D(0, 0, gameController.inGameCamera.camera.viewportWidth,
                                                        gameController.inGameCamera.camera.viewportHeight);

        gameController.assetManager.viewport.update(width, height);

        gameController.uiCamera.position.set(gameController.uiCamera.viewportWidth / 2,
                    gameController.uiCamera.viewportHeight / 2, 10);
        gameController.uiCamera.update();
    }

    @Override
    public void hide() {
        ambienceMusic.stop();
    }

    @Override
    public void dispose() {

    }


    public void populateUITable() {
        weaponChanged = false;

        mainTable.clearChildren();
        gameController.assetManager.stage.clear();

        // Add the buttons to the tables.
        mainTable.add().colspan(2);
        mainTable.add(playerNameText).height(10).align(Align.bottomLeft);
        mainTable.add().height(10).colspan(3);
        mainTable.row();
        mainTable.add().width(5);
        mainTable.add(menuButton);
        mainTable.add(livesTable).expandX().left();
        Table infoTextTable = new Table();
        infoTextTable.add(creaturesSlainText).expandX().right();
        infoTextTable.row();
        infoTextTable.add(timerText).expandX().right();
        mainTable.add(infoTextTable);
        mainTable.add(compassTable).colspan(2);
        mainTable.add().width(5);
        mainTable.row();
        mainTable.add().width(5);
        mainTable.add(buffsTable).expandY().align(Align.top);
        mainTable.row();
        mainTable.add().width(5);
        if (gameController.player.attackMode == Enums.AttackType.MELEE) {
            mainTable.add(swapBowButton);
            mainTable.add().fillX().colspan(2);
            mainTable.add(shieldButton);
            mainTable.add(swordButton);
        }
        else {
            mainTable.add(swapSwordButton);
            mainTable.add().fillX().colspan(2);
            mainTable.add().width(48);
            mainTable.add(bowButton);
        }
        mainTable.add().width(5);
        mainTable.row();
        mainTable.add().height(5).fillX().colspan(5);

        // Add the table to the stage.
        gameController.assetManager.stage.addActor(mainTable);
    }

    private void populateMenuTable() {
        gameController.assetManager.stage.clear();

        // Add the table to the stage.
        if (gameController.player.isDead) {
            gameController.assetManager.stage.addActor(deathMenuTable);
        }
        else {
            gameController.assetManager.stage.addActor(menuTable);
        }
    }


    private void createLives() {
        livesTable = new Table();
        lifeImages = new Button[gameController.player.totalLives];
        for (int i = 0; i < gameController.player.lives; i++) {
            lifeImages[i] = new Button(skin, "Life_Active");
        }
        for (int i = gameController.player.lives; i < gameController.player.totalLives; i++) {
            lifeImages[i] = new Button(skin, "Life_Inactive");
        }
        for (int i = 0; i < lifeImages.length; i++) {
            livesTable.add(lifeImages[i]).width(24).height(24);
        }
    }

    private void updateLives() {
        livesTable.clearChildren();
        lifeImages = new Button[gameController.player.totalLives];
        for (int i = 0; i < gameController.player.lives; i++) {
            lifeImages[i] = new Button(skin, "Life_Active");
        }
        for (int i = gameController.player.lives; i < gameController.player.totalLives; i++) {
            lifeImages[i] = new Button(skin, "Life_Inactive");
        }
        for (int i = 0; i < lifeImages.length; i++) {
            livesTable.add(lifeImages[i]).width(24).height(24);
        }
        livesTable.invalidate();
    }

    private void createBuffs() {
        buffsTable = new Table();
        ArrayList<Blessing> permanentBlessings = new ArrayList<Blessing>();
        ArrayList<Integer> permanentBlessingCounts = new ArrayList<Integer>();
        ArrayList<String> permanentBlessingDurations = new ArrayList<String>();
        ArrayList<Blessing> tempBlessings = new ArrayList<Blessing>();
        ArrayList<String> tempBlessingDurations = new ArrayList<String>();
        for (int i = 0; i < gameController.player.blessings.length; i++) {
            Blessing blessing = gameController.player.blessings[i];
            if (blessing != null) {
                String durationString;
                if (blessing.duration == -1) {
                    int blessingIndex = -1;
                    for (int k = 0; k < permanentBlessings.size(); k++) {
                        if (permanentBlessings.get(k).blessingType == blessing.blessingType) {
                            blessingIndex = k;
                            break;
                        }
                    }
                    if (blessingIndex >= 0) {
                        permanentBlessingCounts.set(blessingIndex, permanentBlessingCounts.get(blessingIndex) + 1);
                        permanentBlessingDurations.set(blessingIndex, "*   " + permanentBlessingCounts.get(blessingIndex));
                    }
                    else {
                        permanentBlessings.add(blessing);
                        permanentBlessingCounts.add(1);
                        permanentBlessingDurations.add("*   1");
                    }
                }
                else {
                    BigDecimal durationDecimal = new BigDecimal(Float.toString(blessing.duration));
                    durationDecimal = durationDecimal.setScale(1, BigDecimal.ROUND_HALF_UP);
                    durationString = durationDecimal.toString() + " s";
                    tempBlessings.add(blessing);
                    tempBlessingDurations.add(durationString);
                }
            }
        }

        buffImages = new TextButton[permanentBlessings.size() + tempBlessings.size()];
        for (int i = 0; i < permanentBlessings.size(); i++) {
            buffImages[i] = new TextButton(permanentBlessingDurations.get(i),
                    gameController.assetManager.buffSkin,
                    permanentBlessings.get(i).actorName + 1);
        }

        for (int i = permanentBlessings.size(); i < buffImages.length; i++) {
            int tempBlessingIndex = i - permanentBlessings.size();
            buffImages[i] = new TextButton(tempBlessingDurations.get(tempBlessingIndex),
                    gameController.assetManager.buffSkin,
                    tempBlessings.get(tempBlessingIndex).actorName + 1);
        }

        for (int i = 0; i < buffImages.length; i++) {
            buffsTable.add(buffImages[i]).width(36).height(36);
            if (i != buffImages.length - 1) {
                buffsTable.row();
            }
        }
        buffsTable.invalidate();
    }

    private void updateBuffs() {
        buffsTable.clearChildren();
        ArrayList<Blessing> permanentBlessings = new ArrayList<Blessing>();
        ArrayList<Integer> permanentBlessingCounts = new ArrayList<Integer>();
        ArrayList<String> permanentBlessingDurations = new ArrayList<String>();
        ArrayList<Blessing> tempBlessings = new ArrayList<Blessing>();
        ArrayList<String> tempBlessingDurations = new ArrayList<String>();
        for (int i = 0; i < gameController.player.blessings.length; i++) {
            Blessing blessing = gameController.player.blessings[i];
            if (blessing != null) {
                String durationString;
                if (blessing.duration == -1) {
                    int blessingIndex = -1;
                    for (int k = 0; k < permanentBlessings.size(); k++) {
                        if (permanentBlessings.get(k).blessingType == blessing.blessingType) {
                            blessingIndex = k;
                            break;
                        }
                    }
                    if (blessingIndex >= 0) {
                        permanentBlessingCounts.set(blessingIndex, permanentBlessingCounts.get(blessingIndex) + 1);
                        permanentBlessingDurations.set(blessingIndex, "*   " + permanentBlessingCounts.get(blessingIndex));
                    }
                    else {
                        permanentBlessings.add(blessing);
                        permanentBlessingCounts.add(1);
                        permanentBlessingDurations.add("*   1");
                    }
                }
                else {
                    BigDecimal durationDecimal = new BigDecimal(Float.toString(blessing.duration));
                    durationDecimal = durationDecimal.setScale(1, BigDecimal.ROUND_HALF_UP);
                    durationString = durationDecimal.toString() + " s";
                    tempBlessings.add(blessing);
                    tempBlessingDurations.add(durationString);
                }
            }
        }

        buffImages = new TextButton[permanentBlessings.size() + tempBlessings.size()];
        for (int i = 0; i < permanentBlessings.size(); i++) {
            buffImages[i] = new TextButton(permanentBlessingDurations.get(i),
                    gameController.assetManager.buffSkin,
                    permanentBlessings.get(i).actorName + 1);
        }

        for (int i = permanentBlessings.size(); i < buffImages.length; i++) {
            int tempBlessingIndex = i - permanentBlessings.size();
            buffImages[i] = new TextButton(tempBlessingDurations.get(tempBlessingIndex),
                    gameController.assetManager.buffSkin,
                    tempBlessings.get(tempBlessingIndex).actorName + 1);
        }

        for (int i = 0; i < buffImages.length; i++) {
            buffsTable.add(buffImages[i]).width(36).height(36);
            if (i != buffImages.length - 1) {
                buffsTable.row();
            }
        }
        buffsTable.invalidate();
    }

    private void updateCreaturesSlain() {
        creaturesSlainText.setText("Creatures Slain - " + gameController.player.creaturesKilled);
        creaturesSlainText.invalidate();
    }

    private void updateTimer(float delta) {
        int seconds;
        int minutes;
        int hours;

        hours = (int)(delta / 3600);
        minutes = (int)((delta % 3600) / 60);
        seconds = (int)(((delta % 3600) % 60));

        String timerString = "Time - ";
        if (hours < 10) {
            timerString += "0" + hours;
        }
        else {
            timerString += hours;
        }
        timerString += ":";
        if (minutes < 10) {
            timerString += "0" + minutes;
        }
        else {
            timerString += minutes;
        }
        timerString += ":";
        if (seconds < 10) {
            timerString += "0" + seconds;
        }
        else {
            timerString += seconds;
        }

        timerText.setText(timerString);
        timerText.invalidate();
    }

    public void updateCompass(int rotation) {
        compassTable.setRotation(rotation);
        compassTable.invalidate();
    }

    private void updateWeapons() {

    }

    private void updateIconSizes(Actor[] icons) {
        for (int i = 0; i < icons.length; i++) {
        }
    }
}
