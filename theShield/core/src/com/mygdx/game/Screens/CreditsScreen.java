package com.mygdx.game.Screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.mygdx.game.Controllers.GameController;

/**
 * Created by Solist on 26/03/2017.
 */

public class CreditsScreen extends GameScreen implements Screen {

    public CreditsScreen(GameController gameController) {
        this.gameController = gameController;

        skin = gameController.assetManager.menuSkin;

        sprites = gameController.assetManager.menuSprites;
    }

    @Override
    public void show() {
        gameController.assetManager.setStage(gameController.assetManager.menuSprites);

        // Create a table.
        Table mainTable = new Table();
        // Make the table fill the stage.
        mainTable.setFillParent(true);
        // Set the alignment of the tables contents.
        mainTable.top();

        // Create the screen's title.
        Label creditsTitle = new Label("Credits", skin, "label_Large");

        // Create the screen's content.
        String creditsString = "Credits go to:\n" +
                                                "\n" +
                                                "Project Lead - Tristyn Mackay\n" +
                                                "\n" +
                                                "Lead Developer - Tristyn Mackay\n" +
                                                "\n" +
                                                "Lead Programmer - Tristyn Mackay\n" +
                                                "Assistant Programmer - Ying Cham Choi\n" +
                                                "Assistant Programmer - KinTIk Yau\n" +
                                                "\n" +
                                                "Lead Artist - Genevieve de Vries\n" +
                                                "\n" +
                                                "Lead Music Producer - Joel LaVars-Camburn\n" +
                                                "Lead Sound Producer - Joel LaVars-Camburn";
        Label creditsLabel = new Label(creditsString, skin);

        ScrollPane scrollPane = new ScrollPane(creditsLabel, skin);

        // Create the screen's buttons.
        TextButton backButton = new TextButton("Back", skin, "textButton");

        backButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                ((Game) Gdx.app.getApplicationListener()).setScreen(gameController.mainMenuScreen);
            }
        });

        // Add the content to the tables.
        mainTable.add(creditsTitle).expandX().top().colspan(4);
        mainTable.row();
        mainTable.add(scrollPane).expand().colspan(4);
        mainTable.row();
        mainTable.add().height(10).expandX().colspan(4);
        mainTable.row();
        mainTable.add().width(20);
        mainTable.add(backButton).left().width(100);
        mainTable.add().right().width(100);
        mainTable.add().width(20);

        // Add the table to the stage.
        gameController.assetManager.stage.addActor(mainTable);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0.0f, 0.0f, 0.0f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        // Music loop
        if (gameController.assetManager.menuMusic.getPosition() >= 32.3f) {
            gameController.assetManager.menuMusic.setPosition(8.2f);
        }

        //displayBackground(gameController.assetManager.backgroundSprite,
        //                    gameController.assetManager.backgroundTexture);

        gameController.assetManager.stage.act();
        gameController.assetManager.stage.draw();
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {

    }


    @Override
    public void resize(int width, int height) {
        gameController.assetManager.viewport.update(width, height);
        gameController.uiCamera.position.set(gameController.uiCamera.viewportWidth / 2,
                                            gameController.uiCamera.viewportHeight / 2, 0);
        gameController.uiCamera.update();
    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
