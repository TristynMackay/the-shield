package com.mygdx.game.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.mygdx.game.DataStructures.GameObject;

/**
 * Created by Solist on 26/03/2017.
 */

public abstract class GameScreen extends GameObject {
    protected SpriteBatch sprites;
    protected Skin skin;


    protected void displayBackground(SpriteBatch backgroundSprite, Texture backgroundTexture) {
        backgroundSprite.begin();
        backgroundSprite.draw(backgroundTexture,
                                (Gdx.graphics.getWidth() / 2) - (Gdx.graphics.getHeight() / 2), 0,
                                Gdx.graphics.getHeight(), Gdx.graphics.getHeight());
        backgroundSprite.end();
    }
}
