package com.mygdx.game.Doors;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.game.Controllers.GameController;
import com.mygdx.game.DataStructures.Actor;

/**
 * Created by Solist on 25/03/2017.
 */

public abstract class Door extends Actor {
    public Door(GameController gameController, Texture texture){
        super(texture, new Vector2(128, 128));

        this.gameController = gameController;
        type = ActorType.DOOR;
    }
}
