package com.mygdx.game.Blessings;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.mygdx.game.Controllers.GameController;
import com.mygdx.game.Levels.Level;

/**
 * Created by Solist on 28/05/2017.
 */

public class Blessing_Longevity extends Blessing {

    public Blessing_Longevity(GameController gameController, int buffNumber,
                              Level level, int cellX, int cellY, Boolean used) {
        super(gameController, BlessingType.LONGEVITY, "Buff_Longevity" + buffNumber,
                new Texture(Gdx.files.internal("level/blessing/Blessing_Longevity.png")), used, level);

        actorName = "Longevity";

        playerSounds = new Sound[1][1];
        playerSounds[0][0] = Gdx.audio.newSound(Gdx.files.internal("sound/buff/USE_Longevity.wav"));

        this.cellX = cellX;
        this.cellY = cellY;

        healing = 1;
        lives = 1;
    }
}
