package com.mygdx.game.Blessings;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.game.Characters.Player;
import com.mygdx.game.Controllers.GameController;
import com.mygdx.game.DataStructures.Actor;
import com.mygdx.game.DataStructures.PlayingActor;
import com.mygdx.game.Levels.Level;

/**
 * Created by Solist on 25/03/2017.
 */

public abstract class Blessing extends Actor {

    // Animation.
    public float animationSpeed = 0.5f;
    public float currentAnimationTime = 0;

    // Stats.
    public int lives;
    public int healing;
    public int movementSpeed;
    public int attackSpeed;
    public int damage;
    public int range;

    public float duration = -1;

    public Boolean used = false;

    public int cellX;
    public int cellY;

    public BlessingType blessingType;

    public enum BlessingType {
        ALACRITY,
        AGILITY,
        BERSERKER,
        JUGGERNAUT,
        LONGEVITY,
        SWEEP
    }

    public Blessing(GameController gameController, BlessingType blessingType, String blessingName,
                    Texture texture, Boolean used, Level level) {
        super(texture, new Vector2(128, 128));

        this.gameController = gameController;

        playerObject = (RectangleMapObject)level.buffLayer.getObjects().get(blessingName);
        playerObject.getRectangle().setCenter(playerObject.getRectangle().x + 128, playerObject.getRectangle().y + 128);
        Position.x = playerObject.getRectangle().x;
        Position.y = playerObject.getRectangle().y;

        this.used = used;

        type = Actor.ActorType.BUFF;

        this.blessingType = blessingType;

        Direction = Direction.NORTH;
    }


    public void useBuff(PlayingActor actor) {
        if (used == false) {
            if (actor.type == actor.type.PLAYER) {
                actor.addBlessing(this);
            }

            used = true;
        }
    }


    public boolean render(float delta, SpriteBatch batch) {
        super.render(delta);

        currentAnimationTime += delta;

        if (used == false) {
            float currentFrame = currentAnimationTime % animationSpeed;
            float frameLength = animationSpeed / animations[currentSpriteIndex].length;
            for (int i = 0; i < animations[currentSpriteIndex].length; i++) {
                if (currentFrame >= i * frameLength) {
                    playerSprite = new Sprite(animations[currentSpriteIndex][i]);
                }
            }

            playerSprite.setSize(128, 128);

            playerSprite.setCenter(Position.x, Position.y);
            playerSprite.setRotation(-gameController.inGameCamera.rotation);

            playerSprite.draw(batch);
            return true;
        }
        else {
            float effectVolume = getEffectVolume();
            // Play depleting sound.
            long depletionSoundID = playerSounds[0][0].play();
            playerSounds[0][0].setVolume(depletionSoundID, effectVolume);

            // Play depleting animation.

            // Check current time against depletion time.
            return false;
        }
    }
}
