package com.mygdx.game.Blessings;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.mygdx.game.Controllers.GameController;
import com.mygdx.game.Levels.Level;

/**
 * Created by Solist on 28/05/2017.
 */

public class Blessing_Restoration extends Blessing {

    public Blessing_Restoration(GameController gameController, int buffNumber,
                              Level level, int cellX, int cellY, Boolean used) {
        super(gameController, BlessingType.LONGEVITY, "Buff_Restoration" + buffNumber,
                new Texture(Gdx.files.internal("level/blessing/Blessing_Restoration.png")), used, level);

        actorName = "Restoration";

        playerSounds = new Sound[1][1];
        playerSounds[0][0] = Gdx.audio.newSound(Gdx.files.internal("sound/buff/USE_Restoration.wav"));

        this.cellX = cellX;
        this.cellY = cellY;

        healing = 2;
        duration = 0;
    }
}
