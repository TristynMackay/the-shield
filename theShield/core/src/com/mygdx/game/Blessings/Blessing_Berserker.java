package com.mygdx.game.Blessings;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.mygdx.game.Controllers.GameController;
import com.mygdx.game.Levels.Level;

/**
 * Created by Solist on 28/05/2017.
 */

public class Blessing_Berserker extends Blessing {

    public Blessing_Berserker(GameController gameController, int buffNumber,
                              Level level, int cellX, int cellY, Boolean used) {
        super(gameController, BlessingType.LONGEVITY, "Buff_Berserker" + buffNumber,
                new Texture(Gdx.files.internal("level/blessing/Blessing_Berserker.png")), used, level);

        actorName = "Berserker";

        playerSounds = new Sound[1][1];
        playerSounds[0][0] = Gdx.audio.newSound(Gdx.files.internal("sound/buff/USE_Berserker.wav"));

        this.cellX = cellX;
        this.cellY = cellY;

        damage = 2;
        duration = 20;
    }
}
