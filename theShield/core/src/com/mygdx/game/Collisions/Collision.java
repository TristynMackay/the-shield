package com.mygdx.game.Collisions;


import com.badlogic.gdx.math.Vector2;
import com.mygdx.game.Blessings.Blessing;
import com.mygdx.game.Characters.Creature;
import com.mygdx.game.Characters.Player;
import com.mygdx.game.Controllers.GameController;
import com.mygdx.game.DataStructures.Actor;
import com.mygdx.game.DataStructures.GameObject;
import com.mygdx.game.Traps.Trap;


/**
 * Created by Solist on 25/03/2017.
 */

public class Collision extends GameObject {


    public Collision(GameController gameController) {
        this.gameController = gameController;
    }

    public Actor.ActorType checkCollision(Vector2 position) {

        //int cellX = 64 - ((int)position.x * 128);
        //int cellY = 64 - ((int)position.y * 128);

        int cellX = ((int)position.x - 64) / 128;
        int cellY = ((int)position.y - 64) / 128;


        if(gameController.level.getWallCell(cellX, cellY) != null){
            return Actor.ActorType.WALL;
        }
        if (gameController.level.getStairCell(cellX, cellY) != null) {
            return Actor.ActorType.STAIR;
        }
        else if(checkPlayerCollision(position) != null){
            return Actor.ActorType.PLAYER;
        }
        else if(checkEnemyCollision(position) != null){
            return Actor.ActorType.ENEMY;
        }
        else if(checkTrapCollision(position) != null){
            return Actor.ActorType.TRAP;
        }
        else if(checkBuffCollision(position) != null){
            return Actor.ActorType.BUFF;
        }
        else if(gameController.level.getProjectileCell(cellX, cellY) != null){
            return Actor.ActorType.PROJECTILE;
        }
        return null;
    }

    public Boolean booleanWallCollision(int cellX, int cellY) {
        if(gameController.level.getWallCell(cellX, cellY) != null){
            return true;
        }
        return false;
    }


    public Boolean checkStairCollision(int cellX, int cellY) {
        if (gameController.level.getStairCell(cellX, cellY) != null) {
            return true;
        }
        return false;
    }


    public Player checkPlayerCollision(Vector2 position) {
        Vector2 playerPosition = gameController.player.Position;

        if (position.x > playerPosition.x - 64 && position.x < playerPosition.x + 64) {
            if (position.y > playerPosition.y - 64 && position.y < playerPosition.y + 64) {
                return gameController.player;
            }
        }
        return null;
    }

    public Creature checkEnemyCollision(Vector2 position) {
        for (int i = 0; i < gameController.level.creatureList.length; i++) {
            if (gameController.level.creatureList[i] != null) {
                if (gameController.level.creatureList[i].isDead == false) {
                    Vector2 creaturePosition = gameController.level.creatureList[i].Position;

                    if (position.x > creaturePosition.x - 64 && position.x < creaturePosition.x + 64) {
                        if (position.y > creaturePosition.y - 64 && position.y < creaturePosition.y + 64) {
                            return gameController.level.creatureList[i];
                        }
                    }
                }
            }
        }

        return null;
    }

    public Blessing checkBuffCollision(Vector2 position) {
        for (int i = 0; i < gameController.level.buffList.length; i++) {
            if (gameController.level.buffList[i] != null) {
                Vector2 buffPosition = gameController.level.buffList[i].Position;

                if (position.x > buffPosition.x - 64 && position.x < buffPosition.x + 64) {
                    if (position.y > buffPosition.y - 64 && position.y < buffPosition.y + 64) {
                        return gameController.level.buffList[i];
                    }
                }
            }
        }

        return null;
    }

    public Trap checkTrapCollision(Vector2 position) {
        for (int i = 0; i < gameController.level.trapList.length; i++) {
            if (gameController.level.trapList[i] != null) {
                Vector2 trapPosition = gameController.level.trapList[i].Position;

                if (position.x > trapPosition.x - 64 && position.x < trapPosition.x + 64) {
                    if (position.y > trapPosition.y - 64 && position.y < trapPosition.y + 64) {
                        return gameController.level.trapList[i];
                    }
                }
            }
        }

        return null;
    }
}
