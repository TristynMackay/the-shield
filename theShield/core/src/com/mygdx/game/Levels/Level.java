package com.mygdx.game.Levels;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapLayers;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapRenderer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTileSet;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.game.Blessings.Blessing;
import com.mygdx.game.Characters.Creature;
import com.mygdx.game.Controllers.GameController;
import com.mygdx.game.DataStructures.Enums;
import com.mygdx.game.DataStructures.GameObject;
import com.mygdx.game.Projectiles.Projectile;
import com.mygdx.game.Stairs.Stair;
import com.mygdx.game.Traps.Trap;

/**
 * Created by Solist on 31/03/2017.
 */

public class Level extends GameObject {

    public int mapWidth;
    public int mapHeight;

    public TiledMap map;
    public TiledMapTileSet floorTiles;
    public TiledMapTileSet wallTiles;
    public TiledMapTileSet propTiles;
    public TiledMapTileSet projectileTiles;
    public TiledMapTileSet specialEffectTiles;
    public TiledMapTileLayer floorLayer;
    public TiledMapTileLayer stairNorthLayer;
    public TiledMapTileLayer stairEastLayer;
    public TiledMapTileLayer stairSouthLayer;
    public TiledMapTileLayer stairWestLayer;
    public TiledMapTileLayer wallNorthLayer;
    public TiledMapTileLayer wallEastLayer;
    public TiledMapTileLayer wallSouthLayer;
    public TiledMapTileLayer wallWestLayer;
    public TiledMapTileLayer propNorthLayer;
    public TiledMapTileLayer propEastLayer;
    public TiledMapTileLayer propSouthLayer;
    public TiledMapTileLayer propWestLayer;
    public MapLayer trapLayer;
    public MapLayer buffLayer;
    public MapLayer playerLayer;
    public TiledMapTileLayer projectileLayer;
    public TiledMapTileLayer specialEffectLayer;

    public TiledMapRenderer mapRenderer;

    public Stair[] stairList;
    public Trap[] trapList;
    public Blessing[] buffList;
    public Creature[] creatureList;
    public Projectile[] projectileList = new Projectile[100];

    public boolean[][] positionsFlaggedForMoving;

    public int radius = 5;


    public Level (GameController gameController) {
        this.gameController = gameController;
    }

    public void Init(String tileMapName, String floorTileSetName, String wallTileSetName) {
        String mapPathName = Gdx.files.internal("level/" + tileMapName + ".tmx").path();
        map = new TmxMapLoader().load(mapPathName);

        mapRenderer = new OrthogonalTiledMapRenderer(map);

        MapLayers layers = map.getLayers();

        floorLayer = (TiledMapTileLayer) layers.get("Floors");
        stairNorthLayer = (TiledMapTileLayer) layers.get("Stairs_NORTH");
        stairEastLayer = (TiledMapTileLayer) layers.get("Stairs_EAST");
        stairSouthLayer = (TiledMapTileLayer) layers.get("Stairs_SOUTH");
        stairWestLayer = (TiledMapTileLayer) layers.get("Stairs_WEST");
        wallNorthLayer = (TiledMapTileLayer) layers.get("Walls_NORTH");
        wallEastLayer = (TiledMapTileLayer) layers.get("Walls_EAST");
        wallSouthLayer = (TiledMapTileLayer) layers.get("Walls_SOUTH");
        wallWestLayer = (TiledMapTileLayer) layers.get("Walls_WEST");
        propNorthLayer = (TiledMapTileLayer) layers.get("Props_NORTH");
        propEastLayer = (TiledMapTileLayer) layers.get("Props_EAST");
        propSouthLayer = (TiledMapTileLayer) layers.get("Props_SOUTH");
        propWestLayer = (TiledMapTileLayer) layers.get("Props_WEST");
        trapLayer = layers.get("Objects_Traps");
        buffLayer = layers.get("Objects_Buffs");
        playerLayer = layers.get("Objects_Players");
        projectileLayer = (TiledMapTileLayer) layers.get("Projectiles");
        specialEffectLayer = (TiledMapTileLayer) layers.get("SpecialEffects");

        floorTiles = map.getTileSets().getTileSet(floorTileSetName);
        wallTiles = map.getTileSets().getTileSet(wallTileSetName);
        propTiles = map.getTileSets().getTileSet("Props");
        projectileTiles = map.getTileSets().getTileSet("Projectiles");
        specialEffectTiles = map.getTileSets().getTileSet("SpecialEffects");

        positionsFlaggedForMoving = new boolean[mapWidth][mapHeight];
    }


    public TiledMapTileLayer.Cell getFloorCell(int x, int y) {
        return floorLayer.getCell(x, y);
    }


    public TiledMapTileLayer.Cell getWallCell(int x, int y) {
        if (wallNorthLayer.isVisible()) {
            return wallNorthLayer.getCell(x, y);
        } else if (wallEastLayer.isVisible()) {
            return wallEastLayer.getCell(x, y);
        } else if (wallSouthLayer.isVisible()) {
            return wallSouthLayer.getCell(x, y);
        } else {
            return wallWestLayer.getCell(x, y);
        }
    }


    public TiledMapTileLayer.Cell getStairCell(int x, int y) {
        if (stairNorthLayer.isVisible()) {
            return stairNorthLayer.getCell(x, y);
        } else if (stairEastLayer.isVisible()) {
            return stairEastLayer.getCell(x, y);
        } else if (stairSouthLayer.isVisible()) {
            return stairSouthLayer.getCell(x, y);
        } else {
            return stairWestLayer.getCell(x, y);
        }
    }


    public TiledMapTileLayer.Cell getPropCell(int x, int y) {
        if (propNorthLayer.isVisible()) {
            return propNorthLayer.getCell(x, y);
        } else if (propEastLayer.isVisible()) {
            return propEastLayer.getCell(x, y);
        } else if (propSouthLayer.isVisible()) {
            return propSouthLayer.getCell(x, y);
        } else {
            return propWestLayer.getCell(x, y);
        }
    }


    public RectangleMapObject getTrapCell(String trapName) {
        MapObject trap = trapLayer.getObjects().get(trapName);
        if (trap != null) {
            return (RectangleMapObject)trap;
        }
        return null;
    }


    public RectangleMapObject getBuffCell(String buffName) {
        MapObject buff = buffLayer.getObjects().get(buffName);
        if (buff != null) {
            return (RectangleMapObject)buff;
        }
        return null;
    }


    public RectangleMapObject getPlayerCell(String playerName) {
        MapObject player = playerLayer.getObjects().get(playerName);
        if (player != null && player.getProperties().get("Alignment").equals("Player")) {
            return (RectangleMapObject)player;
        }
        return null;
    }


    public RectangleMapObject getEnemyCell(String playerName) {
        MapObject enemy = playerLayer.getObjects().get(playerName);
        if (enemy != null && enemy.getProperties().get("Alignment").equals("Enemy")) {
            return (RectangleMapObject)enemy;
        }
        return null;
    }


    public TiledMapTileLayer.Cell getProjectileCell(int x, int y) {
        return projectileLayer.getCell(x, y);
    }


    public TiledMapTileLayer.Cell getSpecialEffectCell(int x, int y) {
        return specialEffectLayer.getCell(x, y);
    }


    public TiledMapTileLayer.Cell[] getFloors(Vector2 referencePosition) {
        return getCellsInRange(floorLayer, referencePosition);
    }


    public TiledMapTileLayer.Cell[] getWalls(Vector2 referencePosition) {
        if (wallNorthLayer.isVisible()) {
            return getCellsInRange(wallNorthLayer, referencePosition);
        } else if (wallEastLayer.isVisible()) {
            return getCellsInRange(wallEastLayer, referencePosition);
        } else if (wallSouthLayer.isVisible()) {
            return getCellsInRange(wallSouthLayer, referencePosition);
        } else {
            return getCellsInRange(wallWestLayer, referencePosition);
        }
    }


    public TiledMapTileLayer.Cell[] getProps(Vector2 referencePosition) {
        if (propNorthLayer.isVisible()) {
            return getCellsInRange(propNorthLayer, referencePosition);
        } else if (propEastLayer.isVisible()) {
            return getCellsInRange(propEastLayer, referencePosition);
        } else if (propSouthLayer.isVisible()) {
            return getCellsInRange(propSouthLayer, referencePosition);
        } else {
            return getCellsInRange(propWestLayer, referencePosition);
        }
    }


    public TiledMapTileLayer.Cell[] getProjectiles(Vector2 referencePosition) {
        return getCellsInRange(projectileLayer, referencePosition);
    }


    public TiledMapTileLayer.Cell[] getSpecialEffects(Vector2 referencePosition) {
        return getCellsInRange(specialEffectLayer, referencePosition);
    }


    private TiledMapTileLayer.Cell[] getCellsInRange(TiledMapTileLayer tileLayer, Vector2 referencePosition) {
        int xBeginning = (int) referencePosition.x - radius;
        int xEnding = (int) referencePosition.x + radius;

        int yBeginning = (int) referencePosition.y - radius;
        int yEnding = (int) referencePosition.y + radius;

        if (xBeginning < 0) {
            xBeginning = 0;
        }
        if (xEnding > floorLayer.getWidth() - 1) {
            xEnding = floorLayer.getWidth() - 1;
        }
        if (yBeginning < 0) {
            yBeginning = 0;
        }
        if (yEnding > floorLayer.getWidth() - 1) {
            yEnding = floorLayer.getHeight() - 1;
        }

        TiledMapTileLayer.Cell[] cells = new TiledMapTileLayer.Cell[(1 + xEnding - xBeginning) *
                (1 + yEnding - yBeginning)];

        int index = 0;

        for (int i = xBeginning; i < xEnding; i++) {
            for (int k = yBeginning; k < yEnding; k++) {
                TiledMapTileLayer.Cell cell = tileLayer.getCell(i, k);
                if (cell != null) {
                    cells[index] = cell;
                    index++;
                }
            }
        }

        return cells;
    }


    public void updateLevel(Enums.Direction direction) {
        switch (direction) {
            case NORTH:
                // Set Stairs.
                stairEastLayer.setVisible(false);
                stairSouthLayer.setVisible(false);
                stairWestLayer.setVisible(false);
                stairNorthLayer.setVisible(true);
                // Set walls.
                wallEastLayer.setVisible(false);
                wallSouthLayer.setVisible(false);
                wallWestLayer.setVisible(false);
                wallNorthLayer.setVisible(true);
                // Set props.
                propEastLayer.setVisible(false);
                propSouthLayer.setVisible(false);
                propWestLayer.setVisible(false);
                propNorthLayer.setVisible(true);
                break;
            case EAST:
                // Set walls.
                stairNorthLayer.setVisible(false);
                stairSouthLayer.setVisible(false);
                stairWestLayer.setVisible(false);
                stairEastLayer.setVisible(true);
                // Set walls.
                wallNorthLayer.setVisible(false);
                wallSouthLayer.setVisible(false);
                wallWestLayer.setVisible(false);
                wallEastLayer.setVisible(true);
                // Set props.
                propNorthLayer.setVisible(false);
                propSouthLayer.setVisible(false);
                propWestLayer.setVisible(false);
                propEastLayer.setVisible(true);
                break;
            case SOUTH:
                // Set walls.
                stairNorthLayer.setVisible(false);
                stairEastLayer.setVisible(false);
                stairWestLayer.setVisible(false);
                stairSouthLayer.setVisible(true);
                // Set walls.
                wallNorthLayer.setVisible(false);
                wallEastLayer.setVisible(false);
                wallWestLayer.setVisible(false);
                wallSouthLayer.setVisible(true);
                // Set props.
                propNorthLayer.setVisible(false);
                propEastLayer.setVisible(false);
                propWestLayer.setVisible(false);
                propSouthLayer.setVisible(true);
                break;
            case WEST:
                // Set walls.
                stairNorthLayer.setVisible(false);
                stairEastLayer.setVisible(false);
                stairSouthLayer.setVisible(false);
                stairWestLayer.setVisible(true);
                // Set walls.
                wallNorthLayer.setVisible(false);
                wallEastLayer.setVisible(false);
                wallSouthLayer.setVisible(false);
                wallWestLayer.setVisible(true);
                // Set props.
                propNorthLayer.setVisible(false);
                propEastLayer.setVisible(false);
                propSouthLayer.setVisible(false);
                propWestLayer.setVisible(true);
                break;
            default:
                break;
        }
    }


    public void dispose() {
        map.dispose();
    }
}
