package com.mygdx.game.Levels;

import com.badlogic.gdx.Gdx;
import com.mygdx.game.Blessings.Blessing;
import com.mygdx.game.Blessings.Blessing_Agility;
import com.mygdx.game.Blessings.Blessing_Longevity;
import com.mygdx.game.Characters.Creature;
import com.mygdx.game.Characters.Creatures.Enemy_Bat;
import com.mygdx.game.Controllers.GameController;
import com.mygdx.game.DataStructures.Enums;
import com.mygdx.game.Stairs.Stair;
import com.mygdx.game.Traps.Trap;
import com.mygdx.game.Traps.Trap_Pit;

/**
 * Created by Solist on 3/04/2017.
 */

public class Level_Tutorial extends Level {

    public Level_Tutorial (GameController gameController){
        super(gameController);

        mapWidth = 13;
        mapHeight = 13;

        Init("Level_Tutorial", "Floor_Grey", "Wall_Grey");

        stairList = new Stair[1];
        trapList = new Trap[1];
        buffList = new Blessing[2];
        creatureList = new Creature[1];

        // Stairs.
        stairList[0] = new Stair(gameController, "Level_Normal_1", 0, 2);

        // Traps.
        trapList[0] = new Trap_Pit(gameController, 1, this, 2, 2, false);

        // Buffs.
        buffList[0] = new Blessing_Agility(gameController, 1, this, 10, 6, false);
        buffList[1] = new Blessing_Longevity(gameController, 1, this, 6, 2, false);

        // Creatures.
        creatureList[0] = new Enemy_Bat(gameController, Enums.Direction.EAST, 1, this, 1, 2);

        // Ambience sounds.
        gameController.inGameScreen.ambienceMusic = Gdx.audio.newMusic(
                Gdx.files.internal("sound/ambience/Dungeon_Dark_Ambience.mp3"));
    }
}
