package com.mygdx.game.Levels;

import com.badlogic.gdx.Gdx;
import com.mygdx.game.Blessings.Blessing;
import com.mygdx.game.Blessings.Blessing_Agility;
import com.mygdx.game.Blessings.Blessing_Alacrity;
import com.mygdx.game.Blessings.Blessing_Berserker;
import com.mygdx.game.Blessings.Blessing_Longevity;
import com.mygdx.game.Blessings.Blessing_Restoration;
import com.mygdx.game.Characters.Creature;
import com.mygdx.game.Characters.Creatures.Enemy_Bat;
import com.mygdx.game.Characters.Creatures.Enemy_Bat_FootSoldier;
import com.mygdx.game.Characters.Creatures.Enemy_Bat_Sorcerer;
import com.mygdx.game.Controllers.GameController;
import com.mygdx.game.DataStructures.Enums;
import com.mygdx.game.Stairs.Stair;
import com.mygdx.game.Traps.Trap;
import com.mygdx.game.Traps.Trap_Pit;

/**
 * Created by Solist on 3/04/2017.
 */

public class Level_Normal_1 extends Level {

    public Level_Normal_1 (GameController gameController){
        super(gameController);

        mapWidth = 24;
        mapHeight = 26;

        Init("Level_Normal_1", "Floor_Grey", "Wall_Grey");

        stairList = new Stair[0];
        trapList = new Trap[4];
        buffList = new Blessing[11];
        creatureList = new Creature[9];

        // Stairs.

        // Traps.
        trapList[0] = new Trap_Pit(gameController, 1, this, 21, 17, false);
        trapList[1] = new Trap_Pit(gameController, 2, this, 12, 4, false);
        trapList[2] = new Trap_Pit(gameController, 3, this, 4, 5, false);
        trapList[3] = new Trap_Pit(gameController, 4, this, 4, 8, false);

        // Buffs.
        buffList[0] = new Blessing_Agility(gameController, 1, this, 10, 18, false);
        buffList[1] = new Blessing_Agility(gameController, 2, this, 2, 3, false);
        buffList[2] = new Blessing_Longevity(gameController, 1, this, 2, 15, false);
        buffList[3] = new Blessing_Alacrity(gameController, 1, this, 22, 10, false);
        buffList[4] = new Blessing_Alacrity(gameController, 2, this, 2, 10, false);
        buffList[5] = new Blessing_Berserker(gameController, 1, this, 19, 5, false);
        buffList[6] = new Blessing_Berserker(gameController, 2, this, 7, 3, false);
        buffList[7] = new Blessing_Restoration(gameController, 1, this, 4, 23, false);
        buffList[8] = new Blessing_Restoration(gameController, 2, this, 14, 4, false);
        buffList[9] = new Blessing_Restoration(gameController, 3, this, 21, 1, false);
        buffList[10] = new Blessing_Restoration(gameController, 4, this, 7, 10, false);

        // Creatures.
        creatureList[0] = new Enemy_Bat(gameController, Enums.Direction.WEST, 1, this, 10, 23);
        creatureList[1] = new Enemy_Bat(gameController, Enums.Direction.NORTH, 2, this, 17, 13);
        creatureList[2] = new Enemy_Bat(gameController, Enums.Direction.SOUTH, 3, this, 21, 17);
        creatureList[3] = new Enemy_Bat(gameController, Enums.Direction.WEST, 4, this, 22, 15);
        creatureList[4] = new Enemy_Bat(gameController, Enums.Direction.WEST, 5, this, 22, 6);
        creatureList[5] = new Enemy_Bat(gameController, Enums.Direction.EAST, 6, this, 17, 1);
        creatureList[6] = new Enemy_Bat_FootSoldier(gameController, Enums.Direction.NORTH, 1, this, 22, 1);
        creatureList[7] = new Enemy_Bat_Sorcerer(gameController, Enums.Direction.EAST, 1, this, 2, 15);
        creatureList[8] = new Enemy_Bat_Sorcerer(gameController, Enums.Direction.NORTH, 2, this, 19, 8);

        // Ambience sounds.
        gameController.inGameScreen.ambienceMusic = Gdx.audio.newMusic(
                Gdx.files.internal("sound/ambience/Dungeon_Dark_Ambience.mp3"));
    }
}
