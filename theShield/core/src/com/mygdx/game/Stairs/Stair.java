package com.mygdx.game.Stairs;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.game.Characters.Player;
import com.mygdx.game.Controllers.GameController;
import com.mygdx.game.DataStructures.Actor;

/**
 * Created by YKT on 11/05/2017.
 */

public class Stair extends Actor {
    public String connectedLevelName = "com.mygdx.game.Levels.";

    public Stair(GameController gameController, String connectedLevelName, int cellX, int cellY){ //Texture texture){
        super(new Texture(Gdx.files.internal("level/blessing/Blessing_Agility.png")), new Vector2(128, 128));

        this.gameController = gameController;
        type = ActorType.STAIR;
        this.connectedLevelName += connectedLevelName;

        Position.x = cellX * 128 + 64;
        Position.y = cellY * 128 + 64;
    }

    public void use(Player actor) {
        gameController.nextLevel(connectedLevelName, actor);
    }
}