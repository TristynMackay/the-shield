package com.mygdx.game;

import com.badlogic.gdx.Game;
import com.mygdx.game.Controllers.GameController;


public class TheShield extends Game {

	// Variables.
	private GameController gameController = new GameController();


	@Override
	public void create() {
		gameController.INIT();

		setScreen(gameController.mainMenuScreen);
	}

	@Override
	public void render() {
		super.render();
	}

	@Override
	public void dispose() {

	}
}