package com.mygdx.game.Projectiles;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.game.Controllers.GameController;
import com.mygdx.game.DataStructures.Enums;
import com.mygdx.game.DataStructures.PlayingActor;


/**
 * Created by Solist on 5/06/2017.
 */

public class Arrow extends Projectile {

    public Arrow(GameController gameController, int projectileID, PlayingActor owner, float startTime,
                 Vector2 position, Enums.Direction direction) {
        super(gameController, new Texture(Gdx.files.internal("sprite/projectile/Arrow.png")),
                projectileID, owner, startTime, position, direction, new Vector2(16, 32));

        playerSounds = new Sound[1][1];
        playerSounds[0][0] = Gdx.audio.newSound(Gdx.files.internal("sound/weapon/Hit_Arrow.wav"));

        damage = owner.attackDamage;

        moveSpeed = 0.2f;
    }

    @Override
    public String getClassName() {
        return getClass().getName();
    }
}
