package com.mygdx.game.Projectiles;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.game.Characters.Creature;
import com.mygdx.game.Characters.Player;
import com.mygdx.game.Controllers.GameController;
import com.mygdx.game.DataStructures.Actor;
import com.mygdx.game.DataStructures.Enums;
import com.mygdx.game.DataStructures.PlayingActor;

/**
 * Created by Solist on 4/06/2017.
 */

public class Projectile extends Actor {

    public PlayingActor owner;

    public int projectileID;

    public Vector2 startPosition;

    public float moveSpeed = 0.2f;

    public int damage = 1;

    public Sound hitSound;

    public float startTime;
    public float currentDelayTime;


    Projectile(GameController gameController, Texture texture, int projectileID,
               PlayingActor owner, float startTime, Vector2 position,
               Enums.Direction direction, Vector2 size) {
        super(texture, size);

        this.gameController = gameController;

        this.projectileID = projectileID;

        this.owner = owner;

        Direction = direction;

        Position = position;

        startPosition = position;

        setStartTime(startTime);

        playerSprite = new Sprite(animations[0][0]);

        switch (Direction) {
            case NORTH:
                playerSprite.setRotation(0);
                break;
            case EAST:
                playerSprite.setRotation(270);
                break;
            case SOUTH:
                playerSprite.setRotation(180);
                break;
            case WEST:
                playerSprite.setRotation(90);
                break;
            default:
                break;
        }
    }

    public void MoveForward(float delta) {
        float moveDistance = delta / moveSpeed * 128;


        switch (Direction) {
            case NORTH:
                Position.y += moveDistance;
                break;
            case EAST:
                Position.x += moveDistance;
                break;
            case SOUTH:
                Position.y -= moveDistance;
                break;
            case WEST:
                Position.x -= moveDistance;
                break;
            default:
                break;
        }


        Creature creature = gameController.collisions.checkEnemyCollision(Position);
        if (creature != null && creature.equals(owner) == false) {
            if (creature.isBlocking && creature.blockReady) {
                if (checkTargetFacingUs(creature)) {
                    // Play hit sound.
                    long soundID1 = playerSounds[0][0].play();
                    playerSounds[0][0].setVolume(soundID1, getEffectVolume());
                    // Play the blocked sound.
                    long soundID2 = creature.weaponSounds[1].play();
                    creature.weaponSounds[1].setVolume(soundID2, getEffectVolume());
                } else {
                    // Play hit sound.
                    long soundID = playerSounds[0][0].play();
                    playerSounds[0][0].setVolume(soundID, getEffectVolume());
                    // Harm the player hit.
                    creature.ReceiveDamage(damage, owner);
                }
            } else {
                // Play hit sound.
                long soundID = playerSounds[0][0].play();
                playerSounds[0][0].setVolume(soundID, getEffectVolume());
                // Harm the player hit.
                creature.ReceiveDamage(damage, owner);
            }
            // Play animation.

            // Destroy self.
            gameController.level.projectileList[projectileID] = null;
        }
        Player player = gameController.collisions.checkPlayerCollision(Position);
        if (player != null && player.equals(owner) == false) {
            if (player.isBlocking && player.blockReady) {
                if (checkTargetFacingUs(player)) {
                    // Play the blocked sound.
                    long soundID = player.weaponSounds[1].play();
                    player.weaponSounds[1].setVolume(soundID, getEffectVolume());
                } else {
                    // Play hit sound.
                    long soundID = playerSounds[0][0].play();
                    playerSounds[0][0].setVolume(soundID, getEffectVolume());
                    // Harm the player hit.
                    player.ReceiveDamage(damage, owner);
                }
            } else {
                // Play hit sound.
                long soundID = playerSounds[0][0].play();
                playerSounds[0][0].setVolume(soundID, getEffectVolume());
                // Harm the player hit.
                player.ReceiveDamage(damage, owner);
            }
            // Play animation.

            // Destroy self.
            gameController.level.projectileList[projectileID] = null;

            return;
        }
        float modulus = (Position.x - 64) % 128;
        int cellXPos;
        if (modulus >= 128 / 2) {
            cellXPos = (int)((Position.x - 64 - modulus + 128)) / 128;
        }
        else {
            cellXPos = (int)((Position.x - 64 - modulus)) / 128;
        }

        modulus = (Position.y - 64) % 128;
        int cellYPos;
        if (modulus >= 128 / 2) {
            cellYPos = (int)((Position.x - 64 - modulus + 128)) / 128;
        }
        else {
            cellYPos = (int)((Position.y - 64 - modulus)) / 128;
        }


        if (gameController.collisions.booleanWallCollision(cellXPos, cellYPos) ||
                gameController.collisions.booleanWallCollision(cellXPos, cellYPos)) {
            // Play break sound.
            long soundID = playerSounds[0][0].play();
            playerSounds[0][0].setVolume(soundID, getEffectVolume());
            // Play animation.

            // Destroy self.
            gameController.level.projectileList[projectileID] = null;

            return;
        }
    }


    public boolean checkTargetFacingUs(PlayingActor target) {
        switch(Direction) {
            case NORTH:
                if (target.Direction != Direction.SOUTH) {
                    return false;
                }
                break;
            case EAST:
                if (target.Direction != Direction.WEST) {
                    return false;
                }
                break;
            case SOUTH:
                if (target.Direction != Direction.NORTH) {
                    return false;
                }
                break;
            case WEST:
                if (target.Direction != Direction.EAST) {
                    return false;
                }
                break;
            default:
                break;
        }
        return true;
    }


    public void setStartTime(float time) {
        startTime = time;
        currentDelayTime = time;
    }


    public void render(float delta, SpriteBatch batch) {
        super.render(delta);

        currentDelayTime += delta;

        MoveForward(delta);

        playerSprite.setSize(regionSize.x, regionSize.y);

        playerSprite.setCenter(Position.x, Position.y);

        playerSprite.draw(batch);
    }


    public String getClassName() {
        return getClass().getName();
    }
}
