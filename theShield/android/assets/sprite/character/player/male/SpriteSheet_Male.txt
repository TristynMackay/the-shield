SpriteSheet_Male.png
size: 256, 128
format: RGBA8888
filter: Linear,Linear
repeat: none
Male_Body_Back_BowAndArrow_v2
  rotate: false
  xy: 0, 0
  size: 128, 128
  orig: 128, 128
  offset: 0, 0
  index: -1
Male_Body_Back_SwordAndShield
  rotate: false
  xy: 128, 0
  size: 128, 128
  orig: 128, 128
  offset: 0, 0
  index: -1
