Red_Bat_SpriteSheet.png.png
size: 512, 512
format: RGBA8888
filter: Linear,Linear
repeat: none
Bat_Idle_Back1
  rotate: false
  xy: 0, 0
  size: 128, 128
  orig: 128, 128
  offset: 0, 0
  index: -1
Bat_Idle_Back2
  rotate: false
  xy: 128, 0
  size: 128, 128
  orig: 128, 128
  offset: 0, 0
  index: -1
Bat_Idle_Back3
  rotate: false
  xy: 256, 0
  size: 128, 128
  orig: 128, 128
  offset: 0, 0
  index: -1
Bat_Idle_Back4
  rotate: false
  xy: 384, 0
  size: 128, 128
  orig: 128, 128
  offset: 0, 0
  index: -1
Bat_Idle_Front1
  rotate: false
  xy: 0, 128
  size: 128, 128
  orig: 128, 128
  offset: 0, 0
  index: -1
Bat_Idle_Front2
  rotate: false
  xy: 128, 128
  size: 128, 128
  orig: 128, 128
  offset: 0, 0
  index: -1
Bat_Idle_Front3
  rotate: false
  xy: 256, 128
  size: 128, 128
  orig: 128, 128
  offset: 0, 0
  index: -1
Bat_Idle_Front4
  rotate: false
  xy: 384, 128
  size: 128, 128
  orig: 128, 128
  offset: 0, 0
  index: -1
Bat_Idle_Left1
  rotate: false
  xy: 0, 256
  size: 128, 128
  orig: 128, 128
  offset: 0, 0
  index: -1
Bat_Idle_Left2
  rotate: false
  xy: 128, 256
  size: 128, 128
  orig: 128, 128
  offset: 0, 0
  index: -1
Bat_Idle_Left3
  rotate: false
  xy: 256, 256
  size: 128, 128
  orig: 128, 128
  offset: 0, 0
  index: -1
Bat_Idle_Left4
  rotate: false
  xy: 384, 256
  size: 128, 128
  orig: 128, 128
  offset: 0, 0
  index: -1
Bat_Idle_Right1
  rotate: false
  xy: 0, 384
  size: 128, 128
  orig: 128, 128
  offset: 0, 0
  index: -1
Bat_Idle_Right2
  rotate: false
  xy: 128, 384
  size: 128, 128
  orig: 128, 128
  offset: 0, 0
  index: -1
Bat_Idle_Right3
  rotate: false
  xy: 256, 384
  size: 128, 128
  orig: 128, 128
  offset: 0, 0
  index: -1
Bat_Idle_Right4
  rotate: false
  xy: 384, 384
  size: 128, 128
  orig: 128, 128
  offset: 0, 0
  index: -1
